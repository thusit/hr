<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>



      
            
              <!-- /.card-header -->
              <div class="card-body">
                  <form id="frmusertitle" method="POST" action="{{ route('admin.usertitle.edit.submit') }}" enctype="multipart/form-data">
      @csrf	 
       <input type="hidden" id="id" name="id" value="{{$title->id}}" />
      <input id="company_id" type="hidden"  name="company_id" value="{{ $company_id }}"  >

        <div class="row">
    
          <div class="col-md-12">		  <div class="form-group">
                    <label for="">Parent</label>
                    <select class="form-control  form-control-sm " id="parent_id" name="parent_id">
                        <option value="0">--- Select State ---</option>
                       @if(count($parent_user_title_data) > 0)
                            @foreach($parent_user_title_data as $title_data)
                             <option value="{{$title_data->id}}"  {!! ($title_data->id==$title->parent_id ? "selected=\"selected\"" : "") !!}>{{$title_data->title_name}}</option>
                            @endForeach
                        @else
                             No Record Found
                        @endif
                   
                     
                    </select>
                 </div>
		  </div>
		  
		  </div>
       

		     <div class="row">
          <div class="col-md-12">   <div class="form-group">
                    <label for="e">Title Name</label>
                    <input id="title_name" type="text" class="form-control{{ $errors->has('title_name') ? ' is-invalid' : ' form-control-sm' }}" name="title_name" value="{{ $title->title_name }}"  placeholder="Title Name">
               </div></div>
		     
		  
		  </div>
      	
		  </form>
		

		  
		
		  
		  
            
              <!-- /.card-body -->


        
        

  

