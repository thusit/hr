<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Branch extends Model
{
    use Notifiable;
    use SoftDeletes;
    
    protected $table = 'branch';


    protected $status_all = [10=>'Enable',
                         20=>'Disable',
                        ];
    
    
   public function getStatusAll(){
        return $this->status_all;
    }
    
    
    
    public function departments()
    {
        return $this
            ->belongsToMany('App\Department')
            ->withTimestamps();
    }
    
}
