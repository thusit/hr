<?php

namespace App\Http\Resources\Admin;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
         return [
            'id'  => $this->id,
            'photo_path'  => url('storage/company/'.$this->company_name.'/'.$this->photo_path),
            'company_name'  => $this->company_name,
            'first_name'       => $this->first_name,
            'office_email'      => $this->office_email,
            'office_phone'    => $this->office_phone,
            'created_at' => Carbon::parse($this->created_at)->toDayDateTimeString(),
        ];
    }
}
