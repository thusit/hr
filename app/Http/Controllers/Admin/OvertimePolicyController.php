<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use DateTime;
use Carbon\Carbon;
use Auth;

use App\Http\Resources\Admin\OvertimePolicyResource;

use App\Models\User;
use App\Models\OvertimePolicy;

class OvertimePolicyController extends Controller
{
    //
    
    public function __construct()
    {
            $this->middleware('auth:admin');
    }
    
    public function overtimePolicyList(){
        
        $user = new User();
        $user_data = $user
                
                ->join('roles', 'users.role_id', '=', 'roles.id')
                ->join('companies', 'users.company_id', '=', 'companies.id')
                ->where('companies.id', Auth::user()->company_id)
                ->where('roles.name', '!=' , 'Super Administrator')->get();
        
       return view('admin.policy.list_overtime_policy',['userlist' =>$user_data]);
;
    }
    
    
   public function getOvertimePolicyForDataTable(Request $request){
       
       
       $user_title = new UserTitle();
       $query =    $user_title
                    ->where('company_id', Auth::user()->company_id)
                   // ->where('user_titles','user_titles.id' )
                    ->orderBy($request->column, $request->order);
        
        $user_title = $query->paginate($request->per_page);
        
        return OvertimePolicyResource::collection($user_title);
   }
}
