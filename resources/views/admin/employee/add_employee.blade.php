<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
@extends('admin.admin_template')

@section('content')

<script>
 
</script>


 <div class="card bg-main-content-panel">
        <div class="card-header main-content-panel-header">
          <h3>Employee Contact Information</h3>

        </div>
        <div class="card-body main-content-panel-body  custom-tab">
           <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab">Add/Edit</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab">Tab 2</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab">Tab 3</a></li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
                      Dropdown <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" tabindex="-1" href="#">Action</a>
                      <a class="dropdown-item" tabindex="-1" href="#">Another action</a>
                      <a class="dropdown-item" tabindex="-1" href="#">Something else here</a>
                      <div class="divider"></div>
                      <a class="dropdown-item" tabindex="-1" href="#">Separated link</a>
                    </div>
                  </li>
                </ul>
				
				  <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                     <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
   @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <!-- /.card -->
 <form method="POST" action="{{ route('employee.add.submit') }}" enctype="multipart/form-data">
      @csrf
        <div class="row">
          <div class="col-md-6">

            <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Employee Identification</h3>
				   <div class="card-tools">                
                  <button type="button" class="btn btn-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i> 
					 </button>               
                </div>
              </div>
  <div class="card-body">
			  
<div class="form-group">
                    <label for="">Company</label>
                    Company Name
                  </div>
				  <div class="form-group">
                    <label for="">Status</label>
                   Active
                  </div>
				   <div class="form-row">
    <div class=" col-md-6">
				  <div class="form-group">
                    <label for="e">Employee No</label>
                    <!--<input type="text" class="form-control  form-control-sm " id="" placeholder=""> -->
                    <input id="empno" type="text" class="form-control{{ $errors->has('empno') ? ' is-invalid' : ' form-control-sm' }}" name="empno" value="{{ old('empno') }}" >

                  </div></div>
				   <div class="col-md-6">
				    <div class="form-group">
                    <label for="e">Punch Machine User ID</label>
                     <input id="punch_machine_user_id" type="text" class="form-control{{ $errors->has('punch_machine_user_id') ? ' is-invalid' : ' form-control-sm' }}" name="punch_machine_user_id" value="{{ old('punch_machine_user_id') }}" >

                  </div></div></div>
				  
				        <div class="form-group">
                    <label>Branch</label>
                     <select class="form-control{{ $errors->has('branch_id') ? ' is-invalid' : ' form-control-sm' }}" name="branch_id" id="branch_id" placeholder="Role">
                          <option>--- Select Branch ---</option>
                       @if($branchers->count() > 0)
                            @foreach($branchers as $branch)
                             <option value="{{$branch->id}}">{{$branch->branch_name}}</option>
                            @endForeach
                        @else
                             No Record Found
                        @endif
                     
                    </select>
                  </div>  
				  
				  <div class="form-group">
				     <label>Department</label>
                                     <select class="form-control  form-control-sm " id="department_id" name="department_id">
				
                                      </select>
                  </div>    <div class="form-group">
				     <label>Designation</label>
                    <select class="form-control  form-control-sm ">
                      <option>Head Office</option>
                      <option>Factory</option>
                      <option>Distribution</option>
                     
                    </select>
                  </div> <div class="form-group">
				     <label>Employement Title</label>
                    <select class="form-control  form-control-sm ">
                      <option>Head Office</option>
                      <option>Factory</option>
                      <option>Distribution</option>
                     
                    </select>
                  </div>
				  <div class="form-group">
                    <label for="exampleInputEmail1">Job Skills</label>
                    <input type="etext" class="form-control  form-control-sm " >
                  </div>  <div class="form-group">
				     <label>Policy Group</label>
                    <select class="form-control  form-control-sm ">
                      <option>Head Office</option>
                      <option>Factory</option>
                      <option>Distribution</option>
                     
                    </select>
                  </div>
				  	  <div class="form-group">
                  <label>Appointment Date:</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="fa fa-calendar"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control{{ $errors->has('appointment_date') ? ' is-invalid' : ' form-control-sm' }}" id="appointment_date" name="appointment_date">
                  </div>
                  <!-- /.input group -->
                </div>
				      <!-- textarea -->
                  <div class="form-group">
                    <label>Appointment Note</label>
                    <textarea class="form-control  form-control-sm " id="appointment_note" name="appointment_note" rows="3" placeholder="Enter ..."></textarea>
                  </div>
					  <div class="form-group">
                  <label>Termination Date:</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="fa fa-calendar"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control{{ $errors->has('terminate_date') ? ' is-invalid' : ' form-control-sm' }}" id="terminate_date" name="terminate_date">
                  </div>
                  <!-- /.input group -->
                </div>
				
				      <!-- textarea -->
                  <div class="form-group">
                    <label>Termination Note</label>
                    <textarea class="form-control{{ $errors->has('terminate_note') ? ' is-invalid' : ' form-control-sm' }}" id="terminate_note" name="terminate_note" rows="3" placeholder="Enter ..."></textarea>
                  </div>
				  
				  <div class="form-group">   <label for="">Basis of Employment</label><br>
  <div class="form-row">
             <div class="form-group col-md-7">         
			 
			 <div class="form-check "> 
	 <input class="form-check-input" type="radio" value="option1">
                      <label class="form-check-label">Contract </label> 					  
					  </div>
					 <div class="form-check ">
                      <input class="form-check-input" type="radio" value="option1" >
                      <label class="form-check-label">Training </label> </div>
				
                    <div class="form-check ">
                      <input class="form-check-input" type="radio" value="option1" >
                      <label class="form-check-label">Permanent(with probation) </label>  
                    </div>  
					  </div>
					   <div class="form-group col-md-4">
					   <div class="form-check  "><label for="">Month <select class="form-control  form-control-sm ">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                     
                    </select></label></div>
					</div>
	</div>

               
					

					   <div class="form-check  ">
                      <input class="form-check-input" type="radio" value="option1" >
                      <label class="form-check-label">Permanent(Confirmed) </label>
        </div>
					   <div class="form-check">
                      <input class="form-check-input" type="radio" value="option1" >
                      <label class="form-check-label">Resign </label>
                    </div> 
                  </div>
			<div class="form-group">
                  <label>Date Confirmed:</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="fa fa-calendar"></i>
                      </span>
                    </div>
                     <input type="text" class="form-control{{ $errors->has('confirmed_date') ? ' is-invalid' : ' form-control-sm' }}" id="confirmed_date" name="confirmed_date">
                 </div>
                  <!-- /.input group -->
                </div>
			
			<div class="form-group">
                  <label>Resign Date:</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="fa fa-calendar"></i>
                      </span>
                    </div>
                      <input type="text" class="form-control float-right  form-control-sm " id="resign_date" name="resign_date">
                 </div>
                  <!-- /.input group -->
                </div>
		  <div class="form-row">
    <div class=" col-md-6">
			 <div class="form-group">
				     <label>Currency</label>
                    <select class="form-control  form-control-sm ">
                      <option>LKR</option>
                      <option>USD</option>
                      <option>AUD</option>
                     
                    </select>
                  </div>
				  </div>
		 <div class=" col-md-6">
		 <div class="form-group">
				     <label>Pay Period Schedule</label>
                    <select class="form-control  form-control-sm ">
                      <option>Montly</option>
                      <option>Weekly</option>
                      <option>Daily</option>
                     
                    </select>
                  </div>
				  
				  </div></div>
				  
				  
				  <div class="form-group">
                    <label for="exampleInputEmail1">Permission Group</label>
                    <select class="form-control{{ $errors->has('role_id') ? ' is-invalid' : ' form-control-sm' }}" name="role_id" id="role_id" placeholder="Role">
                       @if($roles->count() > 0)
                            @foreach($roles as $role)
                             <option value="{{$role->id}}">{{$role->name}}</option>
                            @endForeach
                        @else
                             No Record Found
                        @endif
                     
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">User Name</label>
                    <input id="user_name" type="text" class="form-control{{ $errors->has('user_name') ? ' is-invalid' : ' form-control-sm' }}" name="user_name" value="{{ old('user_name') }}"  placeholder="User name">

                  </div>
				  <div class="form-group">
                    <label for="exampleInputEmail1">Password</label>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : ' form-control-sm' }}" name="password" value="{{ old('password') }}"  placeholder="Password">
                  </div>
				  <div class="form-group">
                    <label for="exampleInputEmail1">Re-Enter Password</label>
                    <input id="password_confirmation" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : ' form-control-sm' }}" name="password_confirmation" value="{{ old('password') }}" placeholder="Password">
                  </div>

              

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

         <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Hierarchies</h3>
				   <div class="card-tools">                
                  <button type="button" class="btn btn-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i> 
					 </button>               
                </div>
              </div>
              <div class="card-body">
            
					 <div class="form-group">
				     <label>Request: Missed Punch</label>
                    <select class="form-control  form-control-sm ">
                    <option>----</option>
                      <option>Option1</option>
                      <option>Option2</option>
                     
                    </select>
                  </div>
				  		 <div class="form-group">
				     <label>Request: Time Adjustment</label>
                    <select class="form-control  form-control-sm ">
                     <option>----</option>
                      <option>Option1</option>
                      <option>Option2</option>
                     
                    </select>
                  </div>
				 
				  		 <div class="form-group">
				     <label>Request: Absence (incl. Vacation)</label>
                    <select class="form-control  form-control-sm ">
                    <option>----</option>
                      <option>Option1</option>
                      <option>Option2</option>
                     
                    </select>
                  </div>
				  		 <div class="form-group">
				     <label>Request: Schedule Adjustment</label>
                    <select class="form-control  form-control-sm ">
                      <option>----</option>
                      <option>Option1</option>
                      <option>Option2</option>
                     
                    </select>
                  </div>
				  		 <div class="form-group">
				     <label>Request: Other</label>
                    <select class="form-control  form-control-sm ">
                       <option>----</option>
                      <option>Option1</option>
                      <option>Option2</option>
                     
                    </select>
                  </div>
				  		 <div class="form-group">
				     <label>Exception</label>
                    <select class="form-control  form-control-sm ">
                  <option>----</option>
                      <option>Option1</option>
                      <option>Option2</option>
                     
                    </select>
                  </div>
				  		 <div class="form-group">
				     <label>TimeSheet</label>
                    <select class="form-control  form-control-sm ">
                     <option>----</option>
                      <option>Option1</option>
                      <option>Option2</option>
                     
                    </select>
                  </div>
			 		 <div class="form-group">
				     <label>Permission</label>
                    <select class="form-control  form-control-sm ">
                      <option>----</option>
                      <option>Option1</option>
                      <option>Option2</option>
                     
                    </select>
                  </div>

              </div>
              <!-- /.card-body -->
            </div>  
            <!-- /.card -->

          </div>
          <!-- /.col (left) -->
          <div class="col-md-6">
             <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Contact Information</h3>
				   <div class="card-tools">                
                  <button type="button" class="btn btn-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i> 
					 </button>               
                </div>
              </div>
              <div class="card-body">
    
<div class="form-row">
    <div class="col-md-3 ">
        <div class="form-group"> <label>Title</label>
                    <select name="title" class="form-control  form-control-sm">
                      <option>Mr.</option>
                      <option>Mrs.</option>
                      <option>Ms.</option>
                   
                    </select></div>
    </div>
    <div class="col-md-4 ">  <div class="form-group">
      <label for="validationCustom01">First name</label>
     <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : ' form-control-sm' }}" name="first_name" value="{{ old('first_name') }}"  placeholder="First name">

 </div>
    </div>
    <div class="col-md-5 ">  <div class="form-group">
      <label for="validationCustom02">Last name</label>
      <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '  form-control-sm' }}" name="last_name" value="{{ old('last_name') }}"  placeholder="Last name">

  </div>
    </div></div>
      <div class="form-group">
    <label for="inputAddress2">Name with Initial</label>
    <input id="name_with_initials" type="text" class="form-control{{ $errors->has('name_with_initials') ? ' is-invalid' : '  form-control-sm' }}" name="name_with_initials" value="{{ old('name_with_initials') }}"  placeholder="Name with intials">

  
  </div>  
    <div class="form-group">
    <label for="inputAddress2">NIC No</label>
   <input id="nic" type="text" class="form-control{{ $errors->has('nic') ? ' is-invalid' : '  form-control-sm' }}" name="nic" value="{{ old('nic') }}"  placeholder="NIC No">

  
  </div>
  <div class="form-group">
    <label for="inputAddress2">Date of Birth</label>
 <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="fa fa-calendar"></i>
                      </span>
                    </div>
     <input type="text" class="form-control float-right  form-control-sm " id="dob" name="dob">
                  </div>
  
  </div> 
  
   <div class="form-group">
    <div class="form-row">  
    <div class="col-md-4 "><label>Gender</label>
                    <select class="form-control  form-control-sm" name="gender" id="gender">
                      <option>Male</option>
                      <option>Female</option>
                      
                   
                    </select></div>
	<div class="col-md-4 "><label>Religion</label>
            <select class="form-control  form-control-sm" name="religion" id="religion">
                      <option>Buddhist</option>
                      <option>Christian</option>
                      <option>Islam</option>
		      <option>Hindu</option>
		      <option>Other</option>
                   
                    </select></div>
	<div class="col-md-4 "><label>Marital Status</label>
            <select class="form-control  form-control-sm" name="marital_status" id="marital_status">
                      <option>Married</option>
                      <option>Unmarried</option>
                      <option>Divorced</option>
					  <option>Widowed</option>
                   
                    </select></div>
	</div></div>
  
  <div class="form-group"><label for="validationCustom03">Home Address</label></div>
  <div class="form-group">
    <label for="inputAddress">Address</label>
    <input id="address_1" type="text" class="form-control{{ $errors->has('address_1') ? ' is-invalid' : '  form-control-sm' }}" name="address_1" value="{{ old('address_1') }}"  placeholder="Address">

  </div>
  <div class="form-group">
    <label for="inputAddress2">Address 2</label>
    <input id="address_2" type="text" class="form-control{{ $errors->has('address_2') ? ' is-invalid' : '  form-control-sm' }}" name="address_2" value="{{ old('address_2') }}"  placeholder="Address 2">

  </div>
  <div class="form-row">  
    <div class="col-md-5 ">
      <label for="validationCustom03">City</label>
      <input id="address_city" type="text" class="form-control{{ $errors->has('address_city') ? ' is-invalid' : '  form-control-sm' }}" name="address_city" value="{{ old('address_city') }}"  placeholder="Address City">

      <div class="invalid-feedback">
        Please provide a valid city.
      </div>
    </div>
    <div class="col-md-4 ">
      <label for="validationCustom04">Province/State</label>
      <input id="address_state" type="text" class="form-control{{ $errors->has('address_state') ? ' is-invalid' : '  form-control-sm' }}" name="address_state" value="{{ old('address_state') }}"  placeholder="Address State">

      <div class="invalid-feedback">
        Please provide a valid state.
      </div>
    </div>
    <div class="col-md-3 mb-2">
      <label for="validationCustom05">Postal/Zip</label>
           <input id="address_postal" type="text" class="form-control{{ $errors->has('address_postal') ? ' is-invalid' : '  form-control-sm' }}" name="address_postal" value="{{ old('address_postal') }}"  placeholder="Postal/Zip">

      <div class="invalid-feedback">
        Please provide a valid zip.
      </div>
    </div>
  </div>
  
 <div class="form-group">
    <label >Employee Photo</label>
	<div class="">
            <div id="EmPic"><img src="{{url('storage/'.$profile_pic)}}"  width="100" height="75"/><br><br>
</div>

					 <a href="#">Delete</a><br>
<br>


                    </div>
    <input type="file" class="form-control-file" id="profile_photo" name="profile_photo">
  </div>      <!-- time Picker -->
                
                  <div class="form-group">
                    <label>Home Phone</label>

                    <div class="input-group">
                   <input id="home_phone" type="text" class="form-control{{ $errors->has('home_phone') ? ' is-invalid' : '  form-control-sm' }}" name="home_phone" value="{{ old('home_phone') }}"  placeholder="Home Phone">


                    </div>
                    <!-- /.input group -->
                  </div>
				  <div class="form-group">
                    <label>Mobile Phone</label>

                    <div class="input-group">
                      <input id="mobile_phone" type="text" class="form-control{{ $errors->has('mobile_phone') ? ' is-invalid' : '  form-control-sm' }}" name="mobile_phone" value="{{ old('mobile_phone') }}"  placeholder="Mobile Phone">


                    </div>
                    <!-- /.input group -->
                  </div>
				  <div class="form-group">
                    <label>Personal Email</label>

                    <div class="input-group">
                      <input id="personal_email" type="email" class="form-control{{ $errors->has('personal_email') ? ' is-invalid' : '  form-control-sm' }}" name="personal_email" value="{{ old('personal_email') }}"  placeholder="Personal Email">


                    </div>
                    <!-- /.input group -->
                  </div>
				  <div class="form-group">
                   
  <div class="form-row">  
    <div class="col-md-9 ">
                    <label>Office Phone</label><div class="input-group"> 
                      <input id="office_phone" type="text" class="form-control{{ $errors->has('office_phone') ? ' is-invalid' : '  form-control-sm' }}" name="office_phone" value="{{ old('office_phone') }}"  placeholder="Office Phone">

</div></div> <div class="col-md-3 "><label>Ext: </label><div class="input-group"> 
                         <input id="office_phone_extention" type="text" class="form-control{{ $errors->has('office_phone_extention') ? ' is-invalid' : '  form-control-sm' }}" name="office_phone_extention" value="{{ old('office_phone_extention') }}"  placeholder="Ext">

</div></div>
                    </div>
                    <!-- /.input group -->
                  </div>
				  <div class="form-group">
                    <label>Office Mobile</label>

                    <div class="input-group">
                      <input id="office_mobile" type="text" class="form-control{{ $errors->has('office_mobile') ? ' is-invalid' : '  form-control-sm' }}" name="office_mobile" value="{{ old('office_mobile') }}"  placeholder="Office Mobile">


                    </div>
                    <!-- /.input group -->
                  </div>
				  <div class="form-group">
                    <label>Office Email</label>

                    <div class="input-group">
                     <input id="office_email" type="email" class="form-control{{ $errors->has('office_email') ? ' is-invalid' : '  form-control-sm' }}" name="office_email" value="{{ old('office_email') }}"  placeholder="Office Email">


                    </div>
                    <!-- /.input group -->
                  </div>
				  <div class="form-group">
                    <label>Fax</label>

                    <div class="input-group">
                      <input id="fax" type="text" class="form-control{{ $errors->has('fax') ? ' is-invalid' : '  form-control-sm' }}" name="fax" value="{{ old('fax') }}"  placeholder="Fax">


                    </div>
                    <!-- /.input group -->
                  </div>
		<div class="form-group">
                    <label>City</label>

                    <div class="input-group">
                     <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '  form-control-sm' }}" name="city" value="{{ old('city') }}"  placeholder="city">


                    </div>
                    <!-- /.input group -->
                  </div>
			
				  
		<div class="form-group"> <label>Country</label>
                   <select class="form-control{{ $errors->has('country_id') ? ' is-invalid' : ' form-control-sm' }}" name="country_id" id="country_id" placeholder="Role">
                          <option>--- Select Country ---</option>
                       @if($countries->count() > 0)
                            @foreach($countries as $country)
                             <option value="{{$country->id}}">{{$country->name}}</option>
                            @endForeach
                        @else
                             No Record Found
                        @endif
                     
                    </select>
                </div>
		 <div class="form-group"> <label>Province/State</label>
                    <select class="form-control  form-control-sm" name="state_id" id="state_id">
                     
                   
                    </select></div>
				  <div class="form-group">
                    <label>EPF Registration No</label>

                    <div class="input-group">
                      <input id="epf_registration_no" type="text" class="form-control{{ $errors->has('epf_registration_no') ? ' is-invalid' : '  form-control-sm' }}" name="epf_registration_no" value="{{ old('epf_registration_no') }}"  placeholder="EPF Registration No">


                    </div>
                    <!-- /.input group -->
                  </div>
				  <div class="form-group">
                    <label>EPF Membership No</label>

                    <div class="input-group">
                         <input id="epf_membeship_no" type="text" class="form-control{{ $errors->has('epf_membeship_no') ? ' is-invalid' : '  form-control-sm' }}" name="epf_membeship_no" value="{{ old('epf_membeship_no') }}"  placeholder="EPF Membeship No">


                    </div>
                    <!-- /.input group -->
                  </div>
				  <div class="form-group">
                    <label>Emergency Contact Person</label>

                    <div class="input-group">
                     <input id="emergency_contact_person" type="text" class="form-control{{ $errors->has('emergency_contact_person') ? ' is-invalid' : '  form-control-sm' }}" name="emergency_contact_person" value="{{ old('emergency_contact_person') }}"  placeholder="Emergency contact person">


                    </div>
                    <!-- /.input group -->
                  </div>
				  <div class="form-group">
                    <label>Emergency Contact No</label>

                    <div class="input-group">
                      <input id="emergency_contact_number" type="text" class="form-control{{ $errors->has('emergency_contact_number') ? ' is-invalid' : '  form-control-sm' }}" name="emergency_contact_number" value="{{ old('emergency_contact_number') }}"  placeholder="Emergency contact number">


                    </div>
                    <!-- /.input group -->
                  </div>
				    <div class="form-group"> 
				     <table class="table table-bordered">
          <!--     <tr>
                    <th style="width: 10px">#</th>
                    <th>Task</th>
                    <th>Progress</th>
                    <th style="width: 40px">Label</th>
                  </tr>-->
                  <tr>
                    <td><label>Templates</label></td>
                    <td><label class="file-upload btn2 btn-primary">
                            <span class="fa fa-upload" aria-hidden="true"></span> Upload<input type="file" />
                        </label></td>
                    <td>
                        <div class="list-files">
					<ul><li><a href="#" target="blank">Appontment Letter</a><a href="#"><small class="badge badge-danger"><i class="fa fa-times"></i> </small></a></li></ul></div>
                    </td>
              
                  </tr>
              <tr>
                    <td><label>Perosnal Files</label></td>
                    <td><label class="file-upload btn2 btn-primary">
                            <span class="fa fa-upload" aria-hidden="true"></span> Upload<input type="file" />
                        </label></td>
                    <td>
                        <div class="list-files">
					<ul><li><a href="#" target="blank">File 1</a><a href="#"><small class="badge badge-danger"><i class="fa fa-times"></i> </small></a></li>
					<li><a href="#" target="blank">File 2</a><a href="#"><small class="badge badge-danger"><i class="fa fa-times"></i> </small></a></li>
					<li><a href="#" target="blank">File 3</a><a href="#"><small class="badge badge-danger"><i class="fa fa-times"></i> </small></a></li>
					<li><a href="#" target="blank">File 4</a><a href="#"><small class="badge badge-danger"><i class="fa fa-times"></i> </small></a></li></ul></div>
                    </td>
                 
                  </tr>
				      <tr>
                    <td><label>ID Copy</label></td>
                    <td><label class="file-upload btn2 btn-primary">
                            <span class="fa fa-upload" aria-hidden="true"></span> Upload<input type="file" />
                        </label></td>
                    <td>
                        <div class="list-files">
					<ul><li><a href="#" target="blank">NIC Copy</a><a href="#"><small class="badge badge-danger"><i class="fa fa-times"></i> </small></a></li></ul></div>
                    </td>
               
                  </tr>
				      <tr>
                    <td><label>Birth Certificate</label></td>
                    <td><label class="file-upload btn2 btn-primary">
                            <span class="fa fa-upload" aria-hidden="true"></span> Upload<input type="file" />
                        </label></td>
                    <td>
                        <div class="list-files">
					<ul><li><a href="#" target="blank">BC 1</a><a href="#"><small class="badge badge-danger"><i class="fa fa-times"></i> </small></a></li></ul></div>
                    </td>
                  
                  </tr>
				      <tr>
                    <td><label>GS Letter</label></td>
                    <td><label class="file-upload btn2 btn-primary">
                            <span class="fa fa-upload" aria-hidden="true"></span> Upload<input type="file" />
                        </label></td>
                    <td>
                        <div class="list-files">
					<ul><li><a href="#" target="blank">GS Letter</a><a href="#"><small class="badge badge-danger"><i class="fa fa-times"></i> </small></a></li></ul></div>
                    </td>
             
                  </tr>
				      <tr>
                    <td><label>Police Report</label></td>
                    <td><label class="file-upload btn2 btn-primary">
                            <span class="fa fa-upload" aria-hidden="true"></span> Upload<input type="file" />
                        </label></td>
                    <td>
                        <div class="list-files">
					<ul><li><a href="#" target="blank">Police Report</a><a href="#"><small class="badge badge-danger"><i class="fa fa-times"></i> </small></a></li></ul></div>
                    </td>
              
                  </tr>
				      <tr>
                    <td><label>NDA</label></td>
                    <td><label class="file-upload btn2 btn-primary">
                            <span class="fa fa-upload" aria-hidden="true"></span> Upload<input type="file" />
                        </label></td>
                    <td>
                        <div class="list-files">
					<ul><li><a href="#" target="blank">NDA Copy</a><a href="#"><small class="badge badge-danger"><i class="fa fa-times"></i> </small></a></li></ul></div>
                    </td>
            
                  </tr>
				      <tr>
                    <td><label>Bond</label></td>
                    <td><label class="file-upload btn2 btn-primary">
                            <span class="fa fa-upload" aria-hidden="true"></span> Upload<input type="file" />
                        </label></td>
                    <td>
                        <div class="list-files">
					<ul><li><a href="#" target="blank">Bond</a><a href="#"><small class="badge badge-danger"><i class="fa fa-times"></i> </small></a></li></ul></div>
                    </td>
                  
                  </tr>
                </table>
				  </div>
               
			  
			  <div class="form-row">  
    <div class="col-md-5 ">
			  <div class="form-group">  <label>Bond Period</label>
                    <select class="form-control  form-control-sm">
                      <option>---</option>
                
                   
                    </select></div></div></div>
              <!-- /.card-body -->
            </div></div>
            <!-- /.card -->

      
          </div>
          <!-- /.col (right) -->
        </div>
     
   <!-- /.card-footer-->
        
        <!-- /.row -->
      </div><!-- /.container-fluid -->
      
        <button type="submit" class="btn btn-info float-right"> Save</button>
      
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_2">
                    The European languages are members of the same family. Their separate existence is a myth.
                    For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ
                    in their grammar, their pronunciation and their most common words. Everyone realizes why a
                    new common language would be desirable: one could refuse to pay expensive translators. To
                    achieve this, it would be necessary to have uniform grammar, pronunciation and more common
                    words. If several languages coalesce, the grammar of the resulting language is more simple
                    and regular than that of the individual languages.
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_3">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    It has survived not only five centuries, but also the leap into electronic typesetting,
                    remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                    sheets containing Lorem Ipsum passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum.
                  </div>
                  <!-- /.tab-pane -->
                </div>
		         
		 
		 
		 
		 
        </div>
        <!-- /.card-body -->
        <div class="card-footer main-content-panel-footer">
        
        </div>
      
        
        
        </form>
      </div>
      




@endsection


@section('page-js-files')

@endsection

@section('page-js-script')
<script type="text/javascript">
    $(function () {
    //Initialize Select2 Elements
          

            //Datemask dd/mm/yyyy
            //$('#dob').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
            $('#dob').datepicker({autoclose: true, format:'yyyy-mm-dd',})
            $('#appointment_date').datepicker({autoclose: true,format:'yyyy-mm-dd',}) 
            $('#terminate_date').datepicker({autoclose: true,format:'yyyy-mm-dd',})
            $('#confirmed_date').datepicker({autoclose: true,format:'yyyy-mm-dd',}) 
            $('#resign_date').datepicker({autoclose: true,format:'yyyy-mm-dd',}) 
            
           //////////////////////////////////////////////////////////////////////////////////////////////////////////
           
            $('#country_id').on('change', function(e){
              
                    $.ajaxSetup({
                                    headers: {
                                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                      }
                    });


                    $.ajax({
                            url: '{{ route("admin.ajax.state.bycountry.submit") }}',
                            dataType : 'json',
                            type: 'POST',
                            data: {
                                     _token: '{!! csrf_token() !!}',
                                     country_id: $(this).find('option:selected').val(),
                                 },

                            success:function(response) {
                                 console.log(response);
                                 $("select[name='state_id'").html('');
                                $("select[name='state_id'").html(response.options);
                            }
                       });
            
           });
           
           
           ////////////////////////////////////////////////////////////////////////////////////////////////
           
           $('#branch_id').on('change', function(e){
              
                    $.ajaxSetup({
                                    headers: {
                                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                      }
                    });


                    $.ajax({
                            url: '{{ route("admin.ajax.department.bybranch.select") }}',
                            dataType : 'json',
                            type: 'POST',
                            data: {
                                     _token: '{!! csrf_token() !!}',
                                     branch_id: $(this).find('option:selected').val(),
                                 },

                            success:function(response) {
                                 console.log(response);
                                $("select[name='department_id'").html('');
                                $("select[name='department_id'").html(response.options);
                            }
                       });
            
           });

    });
</script>
@endsection
