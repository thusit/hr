<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Department extends Model
{
    use Notifiable;
    use SoftDeletes;
    
    protected $table = 'department';
    
    protected $status_all = [10=>'Enable',
                             20=>'Disable',
                            ];
    
    
   public function getStatusAll(){
        return $this->status_all;
    }
    
    
    public function branchers()
    {
        return $this
            ->belongsToMany('App\Branch')
            ->withTimestamps();
    }
    
}
