<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        
        $country = [['name' => 'Srilanka','short_code'=>'LK'],
                   [ 'name' => 'India','short_code'=>'IN'],
                   [ 'name' =>'United State','short_code'=>'US']];
        
        DB::table('countries')->insert($country);
         
        
        DB::table('companies')->insert([
            'company_name' => 'Evolve',
            'country_id'=>1,
           
            
          
        ]);
        
        
        DB::table('roles')->insert([
            'name' => 'Super Administrator',
           
            
          
        ]);
        
       DB::table('roles')->insert([
            'name' => 'Administrator',
 
        ]);
        
        DB::table('roles')->insert([
            'name' => 'User', 
        ]);
        
        
        $state_arr = [
            ['country_id' => 1,'state_name' => 'Western province','state_description' => 'Western'],
            ['country_id' => 1,'state_name' => 'Sothen Province','state_description' => 'Sothen'],
             ['country_id' => 1,'state_name' => 'North Province','state_description' => 'North'],
            ];
        
        DB::table('states')->insert($state_arr);
        
         $branch_arr = [
            ['company_id' => 1,'country_id' => 1,'branch_name' => 'Dehiwala','short_code' => '017','code' => '1'],
            ['company_id' => 1,'country_id' => 1,'branch_name' => 'Kollpity','short_code' => '018','code' => '2'],
             ['company_id' => 1,'country_id' => 1,'branch_name' => 'Maharagama','short_code' => '020','code' => '3'],
            ];
        
        DB::table('branch')->insert($branch_arr);
        
        
       $department_arr = [
                ['company_id' => 1,'department_name' => 'HR','department_code' => '1','status'=>10],
                ['company_id' => 1,'department_name' => 'Finance','department_code' => '2','status'=>10],
                ['company_id' => 1,'department_name' => 'Sales','department_code' => '3','status'=>10],
            ];
        
        DB::table('department')->insert($department_arr);
        
        
        
        DB::table('users')->insert([
            'title' => 'Mr',
            'first_name' => 'Evolve',
            'last_name' => 'technology',
            'name_with_initials'=>' Evolve Technology',
            'user_name' => 'thusit',
            'email' => 'thusitfl@gmail.com',
            'password' => bcrypt('abc123'),
            'country_id'=>1,
            'state_id'=>1,
            'branch_id'=>1,
            'department_id'=>1,
            'role_id' => 2,
            'company_id' => 1,
            'photo_path'=>'1.png',
            'appointment_date'=>'2019-04-01',
        ]);
        
        
        
         DB::table('users')->insert([
            'title' => 'Mr',
            'first_name' => 'Super',
            'last_name' => 'Admin',
            'name_with_initials'=>' Super Admin',
            'user_name' => 'super',
            'email' => 'thusitha@gmail.com',
            'password' => bcrypt('abc123'),
            'country_id'=>1,
            'state_id'=>1,
            'branch_id'=>1,
            'department_id'=>1,
            'role_id' => 1,
            'company_id' => 1,
            'photo_path'=>'1.png',
            'appointment_date'=>'2019-04-01',
        ]);
         
       DB::table('role_user')->insert([
            'role_id' => 2, 
            'user_id'=>1
        ]);
       
        DB::table('role_user')->insert([
            'role_id' => 1, 
            'user_id'=>2
        ]);
        
        
        DB::table('admins')->insert([
            'firstname' => 'thusitha',
            'midname' => 'Chandima',
            'lastname' => 'perera',
            'email' => 'thusitha@evolve-sl.com',
            'address' => 'Rathmalana',
            'user_name' => 'thusitha',
            'password' => bcrypt('abc1234'),
          
        ]);
        

        
        
    }
}
