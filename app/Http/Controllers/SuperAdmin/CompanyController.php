<?php

namespace App\Http\Controllers\SuperAdmin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Yajra\Datatables\Datatables;

use App\Models\Company;
use App\models\Country;
use App\Models\User;

class CompanyController extends Controller
{
    //
    
    public function __construct()
    {
            $this->middleware('auth:superadmin');
    }
    
    
    
    public function addForm()
    {
        
        $country = new Country();
        $contry_data= $country->all();
        
        $company = new Company();
        $industry_data = $company->getIndustry();
        
       return view('superadmin.company.add_company',['countries'=>$contry_data,'industry_data'=>$industry_data]);
    }
    
    
    
    public function add(Request $request)
    {
    
         $validator = Validator::make($request->all(), [
            'company_name'   => 'required',
             ]); 
             
        
        if ($validator->fails()) {
            return redirect('superadmin/company/add')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        
        
        $company = new Company();
        $company->industry = $request->industry;
        $company->company_name = $request->company_name;
        $company->short_name = $request->short_name;
        $company->address_1 = $request->address_1;
        $company->address_2 = $request->address_2;
        $company->address_city = $request->address_city;
        $company->country_id = $request->country_id;
        $company->address_state = $request->address_state;
        $company->address_postal = $request->address_postal;
        $company->work_phone = $request->work_phone;
        $company->fax = $request->fax;
        $company->business_identification_number = $request->business_identification_number;
        $company->epf_number = $request->epf_number;
        $company->admin_contact = $request->admin_contact;
        $company->billing_contact = $request->billing_contact;
        $company->support_contact = $request->support_contact;
        $company->enable_second_surname = $request->enable_second_surname; 
        $company->ldpa_authentication_type = $request->ldpa_authentication_type; 
        $company->originator_id = $request->originator_id; 
        $company->data_center_id = $request->data_center_id; 
        
        if(isset($request->company_photo) && !empty($request->company_photo) && $request->file('company_photo')->isValid()){
            $image_name = time();
            $image_name =$image_name.'.png';
            Storage::disk('local')->makeDirectory($company->company_name);
            $path_i = 'public\\company\\'.$company->company_name;
            $imagePath = $request->company_photo->storeAs($path_i, $image_name);
            
            $image = Image::make(Storage::get($imagePath))->resize(200,160)->encode();
            Storage::put($imagePath,$image);
    
            
             $company->logo_path = $image_name;
       }
        
        $company->save();
       
        return redirect('superadmin/company/list');
        
    }
    
    
    
    public function companyList(){
        
        
        $company = new Company();
        $company_list = $company->all();
        
        return view('superadmin.company.list_company',['company_list'=>$company_list]);
    }
    
    
    public function companyListAjax(){
        
        
        $company_o = new Company();
        $company = $company_o->select('id','logo_path','company_name','work_phone','fax');
        
        return Datatables::of($company)
                ->addColumn('image', function ($company) { $url=url('storage/company/'.$company->company_name. '/'.$company->logo_path); 
                             return '<img src="'.$url.'"  border="0" width="40" class="img-rounded" align="center" />'; })
                ->addColumn('action', function ($company) {
                                return '<a href="'. route('superadmin.company.edit.form',$company->id).'" ><small class="badge badge-danger"><i class="fa fa-edit"></i> </small></a>'
                                        .' &nbsp<a href="#"><small class="badge badge-danger"><i class="fa fa-times"></i> </small></a>';
                 })
                ->editColumn('id', 'ID: {{$id}}')
                ->rawColumns(['image','action'])         
                ->make(true);
    }
    
    public function getIndex()
    {
        return view('datatables.id');
    }
    
    public function anyData()
    {
        return Datatables::of($company->select('id','logo_path','company_name','work_phone','fax')->where('id',Auth::user()->company_id))->make(true);
    }
    
    public function editForm($id){
        
        $company = new Company();
        $company_data = $company->find($id);
        
        $country = new Country();
        $contry_data= $country->all();
        
        $user = new User();
        $user_data = $user->where('company_id','=',$company_data->id)->get();
        
        $industry_data = $company->getIndustry();
        
        $data= ['company'=>$company_data,
                'countries'=>$contry_data,
                'user_data'=>$user_data,
                'industry_data'=>$industry_data,
                ];
       return view('superadmin.company.edit_company',$data);
        
    }
    
    
    
    public function edit(Request $request,$id){
        
        $validator = Validator::make($request->all(), [
            'company_name'   => 'required',
             ]); 
        
        
        if ($validator->fails()) {
            return redirect('superadmin/company/edit/'.$request->id)
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $company_o = new Company();
        $company = $company_o->find($request->id);
        
        $company->industry = $request->industry;
        $company->company_name = $request->company_name;
        $company->short_name = $request->short_name;
        $company->address_1 = $request->address_1;
        $company->address_2 = $request->address_2;
        $company->address_city = $request->address_city;
        $company->country_id = $request->country_id;
        $company->address_state = $request->address_state;
        $company->address_postal = $request->address_postal;
        $company->work_phone = $request->work_phone;
        $company->fax = $request->fax;
        $company->business_identification_number = $request->business_identification_number;
        $company->epf_number = $request->epf_number;
        $company->admin_contact = $request->admin_contact;
        $company->billing_contact = $request->billing_contact;
        $company->support_contact = $request->support_contact;
        $company->enable_second_surname = $request->enable_second_surname; 
        $company->ldpa_authentication_type = $request->ldpa_authentication_type; 
        $company->originator_id = $request->originator_id; 
        $company->data_center_id = $request->data_center_id; 
        
        if(isset($request->company_photo) && !empty($request->company_photo) && $request->file('company_photo')->isValid()){
            $image_name = time();
            $image_name =$image_name.'.png';
            Storage::disk('local')->makeDirectory($company->company_name);
            $path_i = 'public\\company\\'.$company->company_name;
            $imagePath = $request->company_photo->storeAs($path_i, $image_name);
            
            $image = Image::make(Storage::get($imagePath))->resize(200,160)->encode();
            Storage::put($imagePath,$image);
    
            
             $company->logo_path = $image_name;
       }
        
        $company->save();
        
        return redirect('superadmin/company/list');
        
    }
    
    
}
