<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
@extends('admin.admin_template')

@section('content')

<script>
 
</script>

 <!-- Content Wrapper. Contains page content -->
    <!-- Main content -->
    <section class="content">
	
		       <div class="card bg-main-content-panel">
        <div class="card-header main-content-panel-header">
          <h3>Branch </h3>

        </div>    
		  <div class="card-body main-content-panel-body">
		  
		  
		
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Branch List</h3>    
              </div>
              <!-- /.card-header -->
              <div class="card-body">
	        <form method="POST" action="{{ route('admin.branch.add.submit') }}" enctype="multipart/form-data">
      @csrf	 
      
      <input id="company_id" type="hidden"  name="company_id" value="{{ $company_id }}"  >

        <div class="row">
    
          <div class="col-md-2">		  <div class="form-group">
                    <label for="">Status</label>
                    <select class="form-control  form-control-sm " id="status" name="status">
                       @if(count($branch_status) > 0)
                            @foreach($branch_status as $key=>$status)
                             <option value="{{$key}}">{{$status}}</option>
                            @endForeach
                        @else
                             No Record Found
                        @endif
                   
                     
                    </select>
                 </div>
		  </div>
		  
		     <div class="col-md-5">	  <div class="form-group">
                    <label for="e">Name</label>
                     <input id="branch_name" type="text" class="form-control{{ $errors->has('branch_name') ? ' is-invalid' : ' form-control-sm' }}" name="branch_name" value="{{ old('branch_name') }}"  placeholder="Branch name">

               </div></div>
			        <div class="col-md-2"> <div class="form-group"><label for="">Branch Short ID</label>   <input id="short_code" type="text" class="form-control{{ $errors->has('short_code') ? ' is-invalid' : ' form-control-sm' }}" name="short_code" value="{{ old('short_code') }}"  placeholder="Branch Code">
</div>
					</div>
					<div class="col-md-3"> <div class="form-group"><label for="">Code</label><input id="code" type="text" class="form-control{{ $errors->has('code') ? ' is-invalid' : ' form-control-sm' }}" name="code" value="{{ old('code') }}"  placeholder="Code"><br>
Next Code:4</div>
					</div>
		  </div>
       
			   
			   	
			   	
				
 
		 
		     <div class="row">
          <div class="col-md-6">   <div class="form-group">
                    <label for="e">Address Line 1</label>
                    <input id="address_1" type="text" class="form-control{{ $errors->has('address_1') ? ' is-invalid' : ' form-control-sm' }}" name="address_1" value="{{ old('address_1') }}"  placeholder="Address 1">
               </div></div>
		     <div class="col-md-6">	  <div class="form-group">
                    <label for="e">Address Line 2</label>
                     <input id="address_2" type="text" class="form-control{{ $errors->has('address_2') ? ' is-invalid' : ' form-control-sm' }}" name="address_2" value="{{ old('address_2') }}"  placeholder="Address 2">
               </div></div>
		  
		  </div>  
		  
		    <div class="row">
          <div class="col-md-6">  <div class="form-group">
                    <label for="e">City</label>
                    <input id="address_city" type="text" class="form-control{{ $errors->has('address_city') ? ' is-invalid' : ' form-control-sm' }}" name="address_city" value="{{ old('address_city') }}"  placeholder="City">
               </div></div>
		     <div class="col-md-6">	  <div class="form-group">
                    <label for="">Country</label>
                      <select class="form-control{{ $errors->has('country_id') ? ' is-invalid' : ' form-control-sm' }}" name="country_id" id="country_id" placeholder="Role">
                          <option>--- Select Country ---</option>
                       @if($countries->count() > 0)
                            @foreach($countries as $country)
                             <option value="{{$country->id}}">{{$country->name}}</option>
                            @endForeach
                        @else
                             No Record Found
                        @endif
                     
                    </select>
                 </div></div>
		  
		  </div>
		      <div class="row">
          <div class="col-md-6"><div class="form-group">
                    <label for="">Province/State</label>
                    <select class="form-control  form-control-sm " id="state_id" name="state_id">
                      
                     
                    </select>
                 </div></div>
		     <div class="col-md-6">   		  
				<div class="form-group">
                    <label for="e">Zip Code</label>
                   <input id="address_postal" type="text" class="form-control{{ $errors->has('address_postal') ? ' is-invalid' : ' form-control-sm' }}" name="address_postal" value="{{ old('address_postal') }}"  placeholder="Zip/Postal">
               
               </div></div>
		  
		  </div>
		       <div class="row">
          <div class="col-md-6"> <div class="form-group">
                    <label for="e">Phone</label>
                     <input id="work_phone" type="text" class="form-control{{ $errors->has('work_phone') ? ' is-invalid' : ' form-control-sm' }}" name="work_phone" value="{{ old('work_phone') }}"  placeholder="phone">
               
               </div>
			   	</div>
		     <div class="col-md-6">  <div class="form-group">
                    <label for="e">Fax</label>
                    <input id="fax" type="text" class="form-control{{ $errors->has('fax') ? ' is-invalid' : ' form-control-sm' }}" name="fax" value="{{ old('fax') }}"  placeholder="Fax">
               
               </div></div>
		  
		  </div>
		  	     <div class="row">  <div class="col-md-6">  <div class="form-group">
                    <label for="e">Business Registration No</label>
                    <input id="business_registration_no" type="text" class="form-control{{ $errors->has('business_registration_no') ? ' is-invalid' : ' form-control-sm' }}" name="business_registration_no" value="{{ old('business_registration_no') }}"  placeholder="Business Registration number">
               
               </div> </div>
          <div class="col-md-6">  <div class="form-group">
                    <label for="e">TIN Number</label>
                     <input id="tin_no" type="text" class="form-control{{ $errors->has('tin_no') ? ' is-invalid' : ' form-control-sm' }}" name="tin_no" value="{{ old('tin_no') }}"  placeholder="Tin No">
               
               </div> </div>
		    
			   </div>
                  
                  	  	     <div class="row">  
		     <div class="col-md-6"> <div class="form-group">
                        <label for="e">EPF Reg Number</label>
                        <input id="epf_no" type="text" class="form-control{{ $errors->has('epf_no') ? ' is-invalid' : ' form-control-sm' }}" name="epf_no" value="{{ old('epf_no') }}"  placeholder="EPF NO">
               
                         </div>
			   
			   </div>
                                         
                             <div class="col-md-6"> <div class="form-group">
                        <label for="e">ETF Reg Number</label>
                        <input id="etf_no" type="text" class="form-control{{ $errors->has('etf_no') ? ' is-invalid' : ' form-control-sm' }}" name="etf_no" value="{{ old('etf_no') }}"  placeholder="ETF NO">
               
                         </div>
			   
			   </div>
			   </div>
		  
		  </div>
		  
		  
              </div>
              <!-- /.card-body -->
             <div class="main-content-panel-footer">
                 <button type="submit" class="btn btn-info float-right"> Save</button>
         
             </form>
        </div>
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
          
          <!-- /.col -->
        </div>
        <!-- /.row -->
        <!-- /.row -->
      </div><!-- /.container-fluid -->
	  
	    </div>
        
        <!-- /.card-body -->
       
        <!-- /.card-footer-->
      
	  
    



@endsection


@section('page-js-files')

@endsection

@section('page-js-script')
<script type="text/javascript">
    $(function () {
    //Initialize Select2 Elements 
        
          $('#country_id').on('change', function(e){
              
            $.ajaxSetup({
                            headers: {
                                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                              }
            });
            
            
            $.ajax({
                    url: '{{ route("admin.ajax.state.bycountry.submit") }}',
                    dataType : 'json',
                    type: 'POST',
                    data: {
                             _token: '{!! csrf_token() !!}',
                             country_id: $(this).find('option:selected').val(),
                         },
                    
                    success:function(response) {
                         console.log(response);
                         $("select[name='state_id'").html('');
                        $("select[name='state_id'").html(response.options);
                    }
               });
            
           });
         

    });
</script>
@endsection
