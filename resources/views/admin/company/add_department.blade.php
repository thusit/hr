<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
@extends('admin.admin_template')

@section('content')

      <div class="card bg-main-content-panel">
        <div class="card-header main-content-panel-header">
          <h3>Department </h3>

        </div>    
		  <div class="card-body main-content-panel-body">
		  
		  
		
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Add Department</h3>    
              </div>
              <!-- /.card-header -->
              <div class="card-body">
	  <form method="POST" action="{{ route('admin.department.add.submit') }}" enctype="multipart/form-data">
      @csrf	 
      
      <input id="company_id" type="hidden"  name="company_id" value="{{ $company_id }}"  >

        <div class="row">
    
          <div class="col-md-6">		  <div class="form-group">
                    <label for="">Status</label>
                    <select class="form-control  form-control-sm " id="status" name="status">
                       @if(count($department_status) > 0)
                            @foreach($department_status as $key=>$status)
                             <option value="{{$key}}">{{$status}}</option>
                            @endForeach
                        @else
                             No Record Found
                        @endif
                   
                     
                    </select>
                 </div>
		  </div>
		  
		  </div>
       
			   
			   	
			   	
				
 
		 
		     <div class="row">
          <div class="col-md-6">   <div class="form-group">
                    <label for="e">Name</label>
                    <input id="department_name" type="text" class="form-control{{ $errors->has('department_name') ? ' is-invalid' : ' form-control-sm' }}" name="department_name" value="{{ old('department_name') }}"  placeholder="Department Name">
               </div></div>
		     
		  
		  </div>
      		     <div class="row">
          
		     <div class="col-md-6">	  <div class="form-group">
                    <label for="e">Code</label>
                     <input id="department_code" type="text" class="form-control{{ $errors->has('department_code') ? ' is-invalid' : ' form-control-sm' }}" name="department_code" value="{{ old('department_code') }}"  placeholder="Code">
               </div></div>
		  
		  </div> 
		  
		    <div class="row">
          
		     <div class="col-md-6">	  <div class="form-group">
                    <label for="">Branch</label>
                      <select class="form-control{{ $errors->has('country_id') ? ' is-invalid' : ' form-control-sm' }}" name="branch[]" id="branch" placeholder="Branch" multiple="multiple">
                       
                       @if($branchers->count() > 0)
                            @foreach($branchers as $branch)
                             <option value="{{$branch->id}}">{{$branch->branch_name}}</option>
                            @endForeach
                        @else
                             No Record Found
                        @endif
                     
                    </select>
                 </div></div>
		  
		  </div>

		  
		  </div>
		  
		  
              </div>
              <!-- /.card-body -->
             <div class="main-content-panel-footer">
                 <button type="submit" class="btn btn-info float-right"> Save</button>
         
             </form>
        </div>
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
          
          <!-- /.col -->
        </div>
        <!-- /.row -->
        <!-- /.row -->
      </div><!-- /.container-fluid -->
	  
	    </div>
        

@endsection


@section('page-js-files')

@endsection

@section('page-js-script')

@endsection
