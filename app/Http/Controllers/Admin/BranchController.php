<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\Branch;
use App\models\Country;
use App\Models\State;

class BranchController extends Controller
{
    //
    public function __construct()
    {
            $this->middleware('auth:admin');
    }
    
    
    
    public function addForm()
    {
        
        $branch = new Branch();
        $branch_status = $branch->getStatusAll();
        
        $countries = Country::all();
        
        $data = [ 'branch_status'=>$branch_status,
                  'countries'=>$countries,
                  'company_id'=> Auth::user()->company_id,
                ];
        
       return view('admin.company.add_branch',$data);
    }
    
    
    public function add(Request $request){
        
        
        $validator = Validator::make($request->all(), [
            'branch_name'   => 'required',
            'short_code'=>'required',
            'code'   => 'required',
            'country_id'   => 'required',
        ]);
        
        
       if ($validator->fails()) {
            return redirect('admin/branch/add')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        
        $branch = new Branch();
        
        $branch->company_id=$request->company_id;
        $branch->branch_name=$request->branch_name;
        $branch->short_code=$request->short_code;
        $branch->code=$request->code;
        $branch->address_1=$request->address_1;
        $branch->address_2=$request->address_2;
        $branch->address_city=$request->address_city;
        $branch->state_id=$request->state_id;
        $branch->address_postal=$request->address_postal;
        $branch->country_id=$request->country_id;
        $branch->work_phone=$request->work_phone;
        $branch->fax=$request->fax;
        $branch->business_registration_no=$request->business_registration_no;
        $branch->epf_no=$request->epf_no;
        $branch->etf_no=$request->etf_no;
        $branch->tin_no=$request->tin_no;
        $branch->status=$request->status;
        
        $branch->save();
        
        return redirect('admin/branch/list');
    }
    
    
    public function branchList(){
          
            $branch = new  Branch();
            DB::enableQueryLog();
            $branch_list = $branch->select('branch.id','branch.branch_name','branch.short_code','states.state_name')
                        ->join('states', 'branch.state_id',  'states.id')
                        ->where('branch.company_id', Auth::user()->company_id)
                        ->get();
        /* $query = DB::getQueryLog();
         $query = end($query);
          print_r($query);
          exit;*/
        
            return view('admin.company.list_branch',['branch_list'=>$branch_list]);
    }
    
    
    
    
    public function editForm($id)
    {
        
        $branch = new Branch();
        $branch_data = $branch->find($id);
        
        $branch_status = $branch->getStatusAll();
        
        $countries = Country::all();
        
        $state_data = State::where('country_id','=',$branch_data->country_id)->get();
        
        $data = [ 'branch_status'=>$branch_status,
                  'countries'=>$countries,
                  'company_id'=> Auth::user()->company_id,
                  'state_data'=>$state_data,
                  'branch'=>$branch_data,
                ];
        
       return view('admin.company.edit_branch',$data);
    }
       
    
    public function edit(Request $request){
        
       
        
        $validator = Validator::make($request->all(), [
            'branch_name'   => 'required',
            'short_code'=>'required',
            'code'   => 'required',
            'country_id'   => 'required',
        ]);
        
        
       if ($validator->fails()) {
            return redirect('admin/branch/edit')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        
         
        $branch_o = new Branch();
        $branch = $branch_o->find($request->id);
        
        $branch->company_id=$request->company_id;
        $branch->branch_name=$request->branch_name;
        $branch->short_code=$request->short_code;
        $branch->code=$request->code;
        $branch->address_1=$request->address_1;
        $branch->address_2=$request->address_2;
        $branch->address_city=$request->address_city;
        $branch->state_id=$request->state_id;
        $branch->address_postal=$request->address_postal;
        $branch->country_id=$request->country_id;
        $branch->work_phone=$request->work_phone;
        $branch->fax=$request->fax;
        $branch->business_registration_no=$request->business_registration_no;
        $branch->epf_no=$request->epf_no;
        $branch->etf_no=$request->etf_no;
        $branch->tin_no=$request->tin_no;
        $branch->status=$request->status;
        
        $branch->save();
        
        return redirect('admin/branch/list');
        
    }
       
       
       
}
