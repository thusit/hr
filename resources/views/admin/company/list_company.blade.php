<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
@extends('admin.admin_template')

@section('content')


                       
	
		       <div class="card bg-main-content-panel">
        <div class="card-header main-content-panel-header">
          <h3>Employees</h3>

        </div>      <div class=" custom-tab"><style>.tab-padding{ padding:2%;}
		</style>
           <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab">Basic Search</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab">Advanced Search</a></li>
                
                  
                </ul>
				
       <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                     <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->

        <!-- /.card -->

        <div class="row">
      <table cellpadding="3" cellspacing="0" border="0" style="width: 67%; margin: 0 auto 2em auto;">
        <thead>
            <tr>
                <th>Target</th>
                <th>Search text</th>
                <th>Treat as regex</th>
                <th>Use smart search</th>
            </tr>
        </thead>
        <tbody>
            <tr id="filter_global">
                <td>Global search</td>
                <td align="center"><input type="text" class="global_filter" id="global_filter"></td>
                <td align="center"><input type="checkbox" class="global_filter" id="global_regex"></td>
                <td align="center"><input type="checkbox" class="global_filter" id="global_smart" checked="checked"></td>
            </tr>
            
            <tr id="filter_col2" data-column="2">
                <td>Company Name</td>
                <td align="center"><input type="text" class="column_filter" id="col2_filter"></td>
                <td align="center"><input type="checkbox" class="column_filter" id="col2_regex"></td>
                <td align="center"><input type="checkbox" class="column_filter" id="col2_smart" checked="checked"></td>
            </tr>
            <tr id="filter_col3" data-column="3">
                <td>Column - Phone</td>
                <td align="center"><input type="text" class="column_filter" id="col3_filter"></td>
                <td align="center"><input type="checkbox" class="column_filter" id="col3_regex"></td>
                <td align="center"><input type="checkbox" class="column_filter" id="col3_smart" checked="checked"></td>
            </tr>
            <tr id="filter_col4" data-column="4">
                <td>Column - Fax</td>
                <td align="center"><input type="text" class="column_filter" id="col4_filter"></td>
                <td align="center"><input type="checkbox" class="column_filter" id="col4_regex"></td>
                <td align="center"><input type="checkbox" class="column_filter" id="col4_smart" checked="checked"></td>
            </tr>
           
        </tbody>
    </table>
             
		  </div>
		  </div>
		  </div>
		  
		        <div class="tab-pane" id="tab_2">
                     <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
   
        <!-- /.card -->

        <div class="row">
          <div class="col-md-12"><div class="tab-padding">Advanced Search</div></div>
		  </div>
		  </div>
		  </div>
		  
		  </div></div>

       <div class="card-body main-content-panel-body">
		  
		  
		
             <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Company List</h3>    <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
     
                  <table class="table table-bordered" id="tblcompany">
                      <thead>
                  <tr>
                   <th>ID</th>
                   <th>Logo</th>
                    <th>Name</th>
                    <th>Phone</th>
                   <th>Fax</th>
                   <th>Action</th>
                  </tr>
                  </thead>
                  <!--
                  @forelse ($company_list as $company)
                        <tr>
                            <td><img src="{{url('storage/company/'.$company->name. '/'.$company->logo_path)}}" class="img-fluid" width="50" height="50"></td>
                          <td>{{$company->company_name}}</td>
                          <td>
                           {{$company->work_phone}}
                          </td>
                                                                                        <td>
                           {{$company->fax}}
                          </td>
                          <td>     
                                                                      <a href="{{ route('admin.company.edit.form',$company->id)}}" target="blank"><small class="badge badge-danger"><i class="fa fa-edit"></i> </small></a> &nbsp;
                                                                      <a href="#"><small class="badge badge-danger"><i class="fa fa-times"></i> </small></a></td>
                        </tr>
               
                  @empty
                    <p>No users</p>
                  @endforelse
                    -->
                </table>    
          
              </div>
              <!-- /.card-body -->
              
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
          
          <!-- /.col -->
        </div>
        <!-- /.row -->
        <!-- /.row -->
      </div><!-- /.container-fluid -->
	  
	</div>




@endsection

@section('page-js-files')

@endsection

@section('page-js-script')
<script type="text/javascript">
    
    
            function filterGlobal () {
                    $('#tblcompany').DataTable().search(
                        $('#global_filter').val(),
                        $('#global_regex').prop('checked'),
                        $('#global_smart').prop('checked')
                    ).draw();
                }
                
            function filterColumn ( i ) {
                    $('#tblcompany').DataTable().column( i ).search(
                        $('#col'+i+'_filter').val(),
                        $('#col'+i+'_regex').prop('checked'),
                        $('#col'+i+'_smart').prop('checked')
                    ).draw();
                } 
    
    
    $(document).ready( function () {
        
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        	}
		});
                
        $('#tblcompany').DataTable(
            {
                processing: true,
                serverSide: true,
                 ajax: {
                        url: '{{ route('admin.company.listajax')}}',
                        type: 'GET',
                        dataFilter: function(data){
                            var json = jQuery.parseJSON( data );
                            console.log(json);
                           // json.recordsTotal = json.total;
                            //json.recordsFiltered = json.total;
                            //json.data = json.list;

                            return JSON.stringify( json ); // return JSON string
                        }},
                        columns: [
                                { data: 'id' , name: 'id'},
                                { data: 'image', name: 'image',render: function( data, type, full, meta ) {
                                                                       return data;} },
                                { data: 'company_name' , name: 'company_name'},
                                { data: 'work_phone' ,name: 'work_phone'},
                                { data: 'fax' ,name: 'fax'},
                                {data: 'action', name: 'action', orderable: false, searchable: false},
                        
                        ],
                        "columnDefs": [
                                {
                                    "targets": [ 0 ],
                                    "visible": false,
                                    "searchable": false
                                },
                                
                            ]
                     
                
            });
            
            
            
            $('input.global_filter').on( 'keyup click', function () {
                    filterGlobal();
                } );
 
            $('input.column_filter').on( 'keyup click', function () {
                    filterColumn( $(this).parents('tr').attr('data-column') );
                } );
            
            
   
                
    } );
 </script>
@endsection