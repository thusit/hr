<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('parent_id')->default(0);
            $table->integer('industry')->nullable();
            $table->string('company_name'); 
            $table->string('short_name')->nullable();
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('address_city')->nullable();
            $table->string('address_state')->nullable();
            $table->string('address_postal')->nullable();
            $table->unsignedBigInteger('country_id');
            $table->string('work_phone')->nullable();
            $table->string('fax')->nullable();
            $table->string('business_identification_number')->nullable();
            $table->string('epf_number')->nullable();
            $table->unsignedBigInteger('admin_contact')->nullable();
            $table->unsignedBigInteger('billing_contact')->nullable();
            $table->unsignedBigInteger('support_contact')->nullable();
            $table->string('logo_path')->nullable();
            $table->decimal('longitude')->nullable();
            $table->decimal('latitude')->nullable();
            $table->tinyInteger('enable_second_surname')->default(0);
            $table->tinyInteger('ldpa_authentication_type')->default(0);
            $table->string('originator_id')->nullable();
            $table->string('data_center_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('country_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}
