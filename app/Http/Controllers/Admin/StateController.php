<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\State;

class StateController extends Controller
{
    //
    
    
    public function getStateByCountry(Request $request)
    {
        
        if($request->ajax()){
            
           
          $states = State::where('country_id','=',$request->country_id)->get();
          $data = view('admin.ajax.select_state',compact('states'))->render();
          return response()->json(['options'=>$data]);
        }
    }
}
