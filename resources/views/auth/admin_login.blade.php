<!--<form id="sign_in_adm" method="POST" action="{{ route('admin.login.submit') }}">
                {{ csrf_field() }}
                
               <div class="input-group"> <span class="input-group-addon"> <i class="zmdi zmdi-account"></i> </span>
                    <div class="form-line">
                        <input type="email" class="form-control" name="email" placeholder="Email Address" value="{{ old('email') }}" required autofocus>
                    </div>
                    @if ($errors->has('email'))
                    <span class="text-danger"><strong>{{ $errors->first('email') }}</strong></span>
                    @endif
                </div>
                
                <div class="input-group"> <span class="input-group-addon"> <i class="zmdi zmdi-lock"></i> </span>
                    <div class="form-line">
                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                    </div>
                </div>
                <div>
                    <div class="">
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} id="rememberme" class="filled-in chk-col-pink">
                        <label for="rememberme">Remember Me</label>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-raised waves-effect g-bg-cyan">SIGN IN</button>
                        
                    </div>
                </div>
            </form>
            </div>
-->


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>EvolveHRM | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
   <link href="{{ asset("/plugins/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  
   <link href="{{ asset("/dist/css/adminlte.min.css") }}" rel="stylesheet" type="text/css" />
   <link href="{{ asset("/dist/css/custom.css") }}" rel="stylesheet" type="text/css" />
   <link href="{{ asset("/dist/css/theme.css") }}" rel="stylesheet" type="text/css" />
  <!-- iCheck -->
  <link href="{{ asset("/plugins/iCheck/square/blue.css") }}" rel="stylesheet" type="text/css" />

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  
  <style>
 /*Custom Login Classes*/
  .admin-top-login{ background:#fff; padding:0.5% 2%;}
  .login-page-bg, .register-page-bg {
    background: #f7f7f0;
}
.login-box-custom, .register-box-custom {
    width: 35%;
    margin: 0% auto;
}
.title_green{ color:#7bbc3c; font-size:22px; line-height: 1.0; font-weight:700;}
.title_dgrey{ color:#212529; font-size:20px; line-height: 1.0; font-weight:600;}
.card_bg { background-color:#f7f7f0!important;box-shadow: 0 0 0px rgba(0,0,0,.125), 0 0px 0px rgba(0,0,0,.2); }
.login-btn{ background-color:#7bbc3c; color:#eaf6de}
.pw-create{ padding:10px 0; }
.pw-create a{color:#7bbc3c; text-decoration:underline;}
.pw-create a:hover,.pw-create a:focus {color:#7bbc3c; text-decoration:none;}
.align-right{ text-align:right}
.align-left{ text-align:left;}
.align-center{ text-align:center}
.padding_top10{ padding:10px 0 0 0;}
.custom-footer-main{/*position: absolute;
    bottom: 0;*/}
.card_body{ padding:0!important;}
.content-login {
  min-height: calc(100vh - 70px);
}
.login-box-msg{ color:#FF0000}
.copyright{background:#5cac0b ;color:#FFFFFF; text-align:center;padding:10px 0;  }
.custom-footer{  background:#5cac0b url(images/footer-img2.png) repeat-x bottom ;
    bottom:0; color:#FFFFFF; text-align:center;  height: 20px; }
.login-logo{margin-bottom:5px;}
@media only screen and (max-width: 650px) {

.login-box-custom, .register-box-custom {
    width: 80%;
    
}}
 /*Custom Login Classes*/
  </style>
</head>
<body class="hold-transition login-page login-page-bg">
  <div class="content-login">
<div class="admin-top-login"><div class="row">
<div class="col-6">
	<span class="title_dgrey"><b><br>
	Evolve HRM</b></span></div>
<div class="col-4 align-right padding_top10"><strong>Language: </strong>  </div>
<div class="col-2">     <div class="">
                  
                    <select class="form-control">
                      <option>English</option>
                      <option>Japanese</option>
                      <option>French</option>
                  
                    </select>
                  </div></div>

</div></div>
<div class="content-wrapper margin-zero">
<div class="login-box-custom">
  <div class="login-logo">
    <a href="../../index2.html"><img src="{{ asset ("/dist/img/Brand/logo-big.png") }}" width="453" height="152"></a>
<br>

<!--<span class="title_green"><strong>Lorem Isupum</strong></span>-->
  </div>
  <!-- /.login-logo -->
  <div class="card ">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Error Message</p>

   <form id="sign_in_adm" method="POST" action="{{ route('admin.login.submit') }}">
                {{ csrf_field() }}
        <div class="form-group has-feedback">
          
         
           <div class="form-line">
                        <input type="email" class="form-control" name="email" placeholder="Email Address" value="{{ old('email') }}" required autofocus>
                    </div>
                    @if ($errors->has('email'))
                    <span class="text-danger"><strong>{{ $errors->first('email') }}</strong></span>
                    @endif
        </div>
        <div class="form-group has-feedback">
          <div class="form-line">
                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                    </div>
         
        </div>
                
                <div class="">
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} id="rememberme" class="filled-in chk-col-pink">
                        <label for="rememberme">Remember Me</label>
                    </div>
                
        <div class="row">
      
          <!-- /.col -->
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
          </div>
          <!-- /.col -->
        </div>
      </form>


      <!-- /.social-auth-links -->
  <div class="row">
          <div class="col-6">
            <div class="pw-create">
        
                  <a href="#">I forgot my password</a>
           
            </div>
          </div>
          <!-- /.col -->
          <div class="col-6">  <div class="pw-create align-right">
             <a href="register.html" class="text-center">Register a new membership</a>
          </div></div>
          <!-- /.col -->
        </div>

    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
</div>


  </div>
    
  <footer class="main-footer margin-zero">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.0-alpha
    </div>
    <strong>Copyright &copy; 2014-2018 <a href="#">Evolve HRM</a>.</strong> All rights
    reserved.
  </footer>

<!-- jQuery -->
<script src="{{ asset ("/plugins/jquery/jquery.min.js") }}"></script>


<!-- jQuery UI 1.11.4 -->

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset ("/plugins/bootstrap/js/bootstrap.bundle.min.js") }}"></script>

<!-- iCheck -->
<script src="{{ asset ("/plugins/iCheck/icheck.min.js") }}"></script>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' // optional
    })
  })
</script>
</body>
</html>


    

