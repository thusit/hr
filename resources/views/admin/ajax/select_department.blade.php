<option>--- Select Department ---</option>
@if(!empty($departments))
  @foreach($departments as $department)
    <option value="{{ $department->id }}">{{ $department->department_name }}</option>
  @endforeach
@endif