<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
@extends('admin.admin_template')

@section('content')

      <div class="card bg-main-content-panel">
        <div class="card-header main-content-panel-header">
          <h3>Department </h3>

        </div>    
		  <div class="card-body main-content-panel-body">
		  
		  
		
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Add User Title</h3>    
              </div>
              <!-- /.card-header -->
              <div class="card-body">
	  <form method="POST" action="{{ route('admin.usertitle.add.submit') }}" enctype="multipart/form-data">
      @csrf	 
      
      <input id="company_id" type="hidden"  name="company_id" value="{{ $company_id }}"  >

        <div class="row">
    
          <div class="col-md-6">		  <div class="form-group">
                    <label for="">Parent</label>
                    <select class="form-control  form-control-sm " id="parent_id" name="parent_id">
                        <option value="0">--- Select State ---</option>
                       @if(count($parent_user_title_data) > 0)
                            @foreach($parent_user_title_data as $title_data)
                             <option value="{{$title_data->id}}">{{$title_data->title_name}}</option>
                            @endForeach
                        @else
                             No Record Found
                        @endif
                   
                     
                    </select>
                 </div>
		  </div>
		  
		  </div>
       

		     <div class="row">
          <div class="col-md-6">   <div class="form-group">
                    <label for="e">Title Name</label>
                    <input id="title_name" type="text" class="form-control{{ $errors->has('title_name') ? ' is-invalid' : ' form-control-sm' }}" name="title_name" value="{{ old('title_name') }}"  placeholder="Title Name">
               </div></div>
		     
		  
		  </div>
      	
		  
		

		  
		  </div>
		  
		  
              </div>
              <!-- /.card-body -->
             <div class="main-content-panel-footer">
                 <button type="submit" class="btn btn-info float-right"> Save</button>
         
             </form>
        </div>
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
          
          <!-- /.col -->
        </div>
        <!-- /.row -->
        <!-- /.row -->
      </div><!-- /.container-fluid -->
	  
	    </div>
        

@endsection


@section('page-js-files')

@endsection

@section('page-js-script')

@endsection
