<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\Branch;
use App\Models\Department;
use App\Models\BranchDepartment;

class DepartmentController extends Controller
{
    //
    
    public function __construct()
    {
            $this->middleware('auth:admin');
    }
    
    
    
    public function addForm()
    {
        
        $department_o = new Department();
        $dep_status = $department_o->getStatusAll();
        
        $branchers = Branch::where('company_id','=',Auth::user()->company_id)->get();
        
        $data = [ 
                  'company_id'=> Auth::user()->company_id,
                  'department_status' =>$dep_status,
                  'branchers'=>$branchers,
                ];
        
       return view('admin.company.add_department',$data);
    }
    
    
    
    
     public function add(Request $request){
         
         
        $validator = Validator::make($request->all(), [
            'department_name'   => 'required',
            'department_code'=>'required',

        ]);
        
        
        if ($validator->fails()) {
            return redirect('admin/department/add')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $department = new Department();
        
        $department->company_id = $request->company_id;
        $department->status = $request->status;
        $department->department_name = $request->department_name;
        $department->department_code = $request->department_code;
        
        $branch_arr  = $request->branch;
        
        $department->save();
        
        foreach($branch_arr as $branch){
           $branch_department = new BranchDepartment();
           
           $branch_department->branch_id = $branch;
           $branch_department->department_id = $department->id;
           
           $branch_department->save();
           
        }
        
        
         return redirect('admin/department/list');
        
     }
    
    
     
    public function departmentList()
    {
        $department_list = Department::where('company_id','=',Auth::user()->company_id)->get();
                
        return view('admin.company.list_department',['department_list'=>$department_list]);
    }
     
    public function editForm($id)
    {
        
        $department_o = new Department();
        $dep_status = $department_o->getStatusAll();
        
        $department = $department_o->find($id);
        
        $branchers = Branch::where('company_id','=',Auth::user()->company_id)->get();
        
        $department_branch =  BranchDepartment::where('department_id','=',$department->id)->get();
        
        $data = [ 
                  'company_id'=> Auth::user()->company_id,
                  'department_status' =>$dep_status,
                  'branchers'=>$branchers,
                  'department'=>$department,
                  'department_branch'=>$department_branch,
                ];
        
       return view('admin.company.edit_department',$data);
    }
    
    
    
    public function edit(Request $request){
        
                 
        $validator = Validator::make($request->all(), [
            'department_name'   => 'required',
            'department_code'=>'required',

        ]);
        
        
        if ($validator->fails()) {
            return redirect('admin/department/edit')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $department_o = new Department();
        $department = $department_o->find($request->id);
        
        $department->company_id = $request->company_id;
        $department->status = $request->status;
        $department->department_name = $request->department_name;
        $department->department_code = $request->department_code;
        
        $branch_arr  = $request->branch;
        
        $department->save();
        
        $branch_department_o = new BranchDepartment();
        $branch_department_o->where('department_id','=',$department->id) 
                              // ->where('branch_id','=',$branch)
                                ->delete();
        
        foreach($branch_arr as $branch){
           
           
            
            
           
           $branch_department = new BranchDepartment();
           
           $branch_department->branch_id = $branch;
           $branch_department->department_id = $department->id;
           
           $branch_department->save();
           
        }
        
        
         return redirect('admin/department/list');
        
    }
    
    
    
    public function getDepartmentByBranch(Request $request)
    {
        
        if($request->ajax()){
            
          $department = new Department();
          $departments = $department->join('department_branch','department_branch.department_id','department.id')
                                    ->join('branch','department_branch.branch_id','branch.id')
                                    ->where('department_branch.deleted_at','=','')
                                    ->orWhereNull('department_branch.deleted_at')
                                    ->where('branch.id',$request->branch_id)
                                    ->get();

          
         
          $data = view('admin.ajax.select_department',compact('departments'))->render();
          return response()->json(['options'=>$data]);
        }
    }
}
