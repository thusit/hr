<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


/*
Route::get('admin', function () {
    return view('admin/admin_template');
});
*/
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home', 'HomeController@index');

Route::get('storage/app/logos/{filename}', function ($filename)
{
    $path = storage_path('public/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});




Route::prefix('admin')->group(function() {
    
    
   Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
   Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
   Route::get('logout/', 'Auth\AdminLoginController@logout')->name('admin.logout');
   Route::get('/', 'AdminController@index')->name('admin.dashboard');
   
   
   ///////COMPANY///////////////////// 
   
   Route::get('/company/list','Admin\CompanyController@companyList')->name('admin.company.list');
   Route::get('/company/listajx','Admin\CompanyController@companyListAjax', [
      'anyData'  => 'datatables.data',
      'getIndex' => 'datatables',
    ])->name('admin.company.listajax');
   Route::get('/company/edit/{id}','Admin\CompanyController@editForm')->name('admin.company.edit.form');
   Route::post('/company/edit/{id}','Admin\CompanyController@edit')->name('admin.company.edit.submit');
   

   
   ///////STATE///////////////
   
   Route::post('/state/bycountry','Admin\StateController@getStateByCountry')->name('admin.ajax.state.bycountry.submit');
   
 
   
   //////EMPLOYEE/////////////
   
   Route::get('/employee/add','Admin\EmployeeController@addForm')->name('admin.employee.add.form');
   Route::post('/employee/add','Admin\EmployeeController@add')->name('admin.employee.add.submit');
   Route::get('/employee/list','Admin\EmployeeController@employeeList')->name('admin.employee.list');
   Route::get('/employee/vuelist','Admin\EmployeeController@getEmployeeForDataTables')->name('admin.employee.vue.list');
   
   //////BRANCH//////////////////
   
   Route::get('/branch/add','Admin\BranchController@addForm')->name('admin.branch.add.form');
   Route::post('/branch/add','Admin\BranchController@add')->name('admin.branch.add.submit');
   Route::get('/branch/list','Admin\BranchController@branchList')->name('admin.branch.list');
   Route::get('/branch/edit/{id}','Admin\BranchController@editForm')->name('admin.branch.edit.form');
   Route::post('/branch/edit','Admin\BranchController@edit')->name('admin.branch.edit.submit');
   
   
   //////////DEPARTMENT////////////// 
   Route::get('/department/add','Admin\DepartmentController@addForm')->name('admin.department.add.form');
   Route::post('/department/add','Admin\DepartmentController@add')->name('admin.department.add.submit');
   Route::get('/department/list','Admin\DepartmentController@departmentList')->name('admin.department.list');
   Route::get('/department/edit/{id}','Admin\DepartmentController@editForm')->name('admin.department.edit.form');
   Route::post('/department/edit','Admin\DepartmentController@edit')->name('admin.department.edit.submit');
   Route::post('/department/selectajax','Admin\DepartmentController@getDepartmentByBranch')->name('admin.ajax.department.bybranch.select');
    
   
   //////////USER TITLE//////////////////// 
   Route::get('/usertitle/add','Admin\UserTitleController@addForm')->name('admin.usertitle.add.form');
   Route::post('/usertitle/add','Admin\UserTitleController@add')->name('admin.usertitle.add.submit');
   Route::get('/usertitle/list','Admin\UserTitleController@userTitleList')->name('admin.usertitle.list');
   Route::get('/usertitle/edit/{id}','Admin\UserTitleController@editForm')->name('admin.usertitle.edit.form');
   Route::post('/usertitle/edit','Admin\UserTitleController@edit')->name('admin.usertitle.edit.submit');
   Route::get('/usertitle/vuelist','Admin\UserTitleController@getUserTitleForDataTable')->name('admin.usertitle.vue.list');
   Route::delete('/usertitle/delete/{id}','Admin\UserTitleController@delete')->name('admin.usertitle.delete');
   
   ////////////POLICY//////////////////////  
   Route::get('/overtimepolicy/vuelist','Admin\OvertimePolicyController@getOvertimePolicyForDataTable')->name('admin.overtime.policy.vue.list');
   Route::get('/overtimepolicy/list','Admin\OvertimePolicyController@overtimePolicyList')->name('admin.overtime.policy.list');
   
  }) ;

///////////////////////////////////SUPER ADMIN/////////////////////////////////////////////////////////////////

  
  Route::prefix('superadmin')->group(function() {
      
      Route::get('/login','Auth\SuperAdminController@showLoginForm')->name('superadmin.login');
      Route::post('/login', 'Auth\SuperAdminController@login')->name('superadmin.login.submit');
      Route::get('logout/', 'Auth\SuperAdminController@logout')->name('superadmin.logout');
      Route::get('/', 'SuperAdminController@index')->name('superadmin.dashboard');
      
      
    //////COMPANY///////////// 
    
   
     Route::get('/company/add','SuperAdmin\CompanyController@addForm')->name('superadmin.company.add.form');
     Route::post('/company/add','SuperAdmin\CompanyController@add')->name('superadmin.company.add.submit');
     Route::get('/company/list','SuperAdmin\CompanyController@companyList')->name('superadmin.company.list');
     Route::get('/company/edit/{id}','SuperAdmin\CompanyController@editForm')->name('superadmin.company.edit.form');
     Route::post('/company/edit/{id}','SuperAdmin\CompanyController@edit')->name('superadmin.company.edit.submit');
     Route::get('/company/listajx','SuperAdmin\CompanyController@companyListAjax', [
      'anyData'  => 'datatables.data',
      'getIndex' => 'datatables',
    ])->name('superadmin.company.listajax');
     
     
     //////EMPLOYEE///////////// 
    
   
     Route::get('/employee/add','SuperAdmin\EmployeeController@addForm')->name('employee.add.form');
     Route::post('/employee/add','SuperAdmin\EmployeeController@add')->name('employee.add.submit');
     Route::get('/employee/list','SuperAdmin\EmployeeController@adminList')->name('employee.superadmin.list');
     Route::get('/employee/edit','SuperAdmin\EmployeeController@editForm')->name('employee.edit.form');
     Route::post('/employee/edit','SuperAdmin\EmployeeController@edit')->name('employee.add.submit');
      
  });