<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    
    
    protected $industry = [ '1'=>'IT//Software',
                             '2'=>'Law', 
                             '3'=>'Logistic',];
    
    
    public function getIndustry(){
        return $this->industry;
    }

}
