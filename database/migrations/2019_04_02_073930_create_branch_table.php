<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id');
            $table->string('branch_name');
            $table->string('short_code');
            $table->string('code');
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('address_city')->nullable();
            $table->unsignedBigInteger('state_id')->nullable();
            $table->string('address_postal')->nullable();
            $table->unsignedBigInteger('country_id');
            $table->string('work_phone')->nullable();
            $table->string('fax')->nullable();
            $table->string('business_registration_no')->nullable();
            $table->string('epf_no')->nullable();
            $table->string('etf_no')->nullable();
            $table->string('tin_no')->nullable();
            $table->decimal('longitude')->nullable();
            $table->decimal('latitude')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('country_id')->references('id')->on('countries');
             $table->foreign('state_id')->references('id')->on('states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch');
    }
}
