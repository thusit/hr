<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
@extends('admin.admin_template')

@section('content')
  
<style>
    
#modal {
  position:absolute;
  left: calc(50% - 100px);
  top: calc(50% - 100px);
  display: flex;
  align-items: center;
  justify-content: center;
  color:white;
  flex-direction: column;
}

.modal {
    position: absolute;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    display: none;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    overflow: hidden;
    position: fixed;
    z-index: 9999;
    width:auto;

}

.modal.is-active {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    width:auto;
}


.modal-background {
    bottom: 0;
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
    background-color: rgba(10,10,10,.86);
}


.modal-close {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -moz-appearance: none;
    -webkit-appearance: none;
    background-color: rgba(10,10,10,.2);
    border: none;
    border-radius: 290486px;
    cursor: pointer;
    display: inline-block;
    -webkit-box-flex: 0;
    -ms-flex-positive: 0;
    flex-grow: 0;
    -ms-flex-negative: 0;
    flex-shrink: 0;
    font-size: 1rem;
    height: 20px;
    max-height: 20px;
    max-width: 20px;
    min-height: 20px;
    min-width: 20px;
    outline: 0;
    position: relative;
    vertical-align: top;
    width: 20px;
    background: 0 0;
    height: 40px;
    position: fixed;
    right: 20px;
    top: 20px;
    width: 40px;
}

.modal-close.is-large {
    height: 32px;
    width: 32px;
}

.modal-close:after, .modal-close:before {
    background-color: #fff;
    content: "";
    display: block;
    left: 50%;
    position: absolute;
    top: 50%;
    -webkit-transform: translateX(-50%) translateY(-50%) rotate(45deg);
    transform: translateX(-50%) translateY(-50%) rotate(45deg);
    -webkit-transform-origin: center center;
    transform-origin: center center;
}

.modal-close:before {
    height: 2px;
    width: 50%;
}
.modal-close:after {
    height: 50%;
    width: 2px;
}


.modal-close:after, .modal-close:before {
    background-color: #fff;
    content: "";
    display: block;
    left: 50%;
    position: absolute;
    top: 50%;
    -webkit-transform: translateX(-50%) translateY(-50%) rotate(45deg);
    transform: translateX(-50%) translateY(-50%) rotate(45deg);
    -webkit-transform-origin: center center;
    transform-origin: center center;
}

.modal-mask {
  position: fixed;
  z-index: 9998;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, .5);
  display: table;
  transition: opacity .3s ease;
}

.modal-wrapper {
  display: flex;
  align-items:center;
  justify-content:center;
  height:100%;
}

.modal-container {
  display:inline-block;
  width: auto;
  margin: 0px auto;
  padding: 20px 30px;
  background-color: #fff;
  border-radius: 2px;
  box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
  transition: all .3s ease;
  font-family: Helvetica, Arial, sans-serif;
}

.modal-header h3 {
  margin-top: 0;
  color: #42b983;
}

.modal-body {
  margin: 20px 0;
}

.modal-default-button {
  float: right;
}

/*
 * The following styles are auto-applied to elements with
 * transition="modal" when their visibility is toggled
 * by Vue.js.
 *
 * You can easily play with the modal transition by editing
 * these styles.
 */

.modal-enter {
  opacity: 0;
}

.modal-leave-active {
  opacity: 0;
}

.modal-enter .modal-container,
.modal-leave-active .modal-container {
  -webkit-transform: scale(1.1);
  transform: scale(1.1);
}

</style>
		       <div class="card bg-main-content-panel">
        <div class="card-header main-content-panel-header">
          <h3>Employees</h3>

        </div>      <div class=" custom-tab"><style>.tab-padding{ padding:2%;}
		</style>
           <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab">Basic Search</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab">Advanced Search</a></li>
                
                  
                </ul>
				
				  <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                     <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->

        <!-- /.card -->

        <div class="row">
          <div class="col-md-12 ">  <div class="tab-padding">Basic Search</div></div>
		  </div>
		  </div>
		  </div>
		  
		        <div class="tab-pane" id="tab_2">
                     <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
   
        <!-- /.card -->

        <div class="row">
          <div class="col-md-12"><div class="tab-padding">Advanced Search</div></div>
		  </div>
		  </div>
		  </div>
		  
		  </div></div>
		  
		  
		  <div class="card-body main-content-panel-body">
		  
		
		
      <div class="container-fluid">  <div class="row">
          <div class="col-md-12">
              <a href="{{ route('admin.usertitle.add.form')}}" > <button type="button" class="btn btn-info float-right btn-margin">+ Add</button></a>
        </div>
<style>.btn-margin{margin:0 0 10px 0;}</style>
</div>
        <div class="row">
          <div class="col-md-12">
	
		  
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Employee Title List</h3>    <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                 
            
            <script type="text/x-template" id="modal-template">
  <transition name="modal">
    <div class="modal-mask">
      <div class="modal-wrapper">
        <div class="modal-container">

          <div class="modal-header">
            <slot name="header">
              default header
            </slot>
          </div>

          <div class="modal-body">
            <slot name="body">
              default body
            </slot>
          </div>

          <div class="modal-footer">
            <slot name="footer">
              default footer
              <button class="modal-default-button" @click="$emit('close')">
                OK
              </button>
            </slot>
          </div>
        </div>
      </div>
    </div>
  </transition>
</script>
            <div class="flex-center position-ref full-height" id="app">
	     <div class="container">
                 
                      	<admin-overtime-policy-data-table
                            fetch-url="{{ route('admin.overtime.policy.vue.list') }}"
                            :columns="['policy_name','overtime_type','Action']"
                         >       </admin-overtime-policy-data-table>
                 
                        
          
                        </div>
          <!--        <button id="show-modal" @click="showModal = true">Show Modal</button>
                         <modal v-if="showModal" @close="showModal = false">
   
      you can use custom content here to overwrite
      default content
    
     <component is="modalComponent"></component>
  </modal>-->
                    </div>
                    <script src="{{ asset('js/app.js') }}"></script>
            
              </div>
              
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
          
          <!-- /.col -->
        </div>
        <!-- /.row -->
        <!-- /.row -->
      </div><!-- /.container-fluid -->
	  
	    </div>
        <!-- /.card-body -->
        <div class="card-footer main-content-panel-footer">
        
        </div>
        <!-- /.card-footer-->
      </div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
               
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>

            <div class="modal-body">

            </div>


        </div>
    </div>
</div>

@endsection


@section('page-js-files')

@endsection

@section('page-js-script')

<script>
    $(document).ready(function(){
      var base_url = window.location.origin;
      $("#myModal").on("show.bs.modal", function(e) {
        var id = $(e.relatedTarget).data('target-id');
        $.get('/admin/usertitle/edit/' + id, function( data ) {
          $(".modal-title").html('Edit Employee Title');
          $(".modal-body").html(data);
        });

      });
    });
  </script>



@endsection
  
    