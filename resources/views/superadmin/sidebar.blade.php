<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<aside class="main-sidebar sidebar-dark-custom elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link bg-side-logo">
      <img src="{{ asset("/dist/img/Brand/Logo-Small.png") }}"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           >
      <span class="brand-text font-weight-light"> Evolve HRM</span><!---->
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset("/dist/img/user2-160x160.jpg") }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
         <li class="nav-header">COMPANY</li>
            <li class="nav-item has-treeview">
		  
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-pie-chart"></i>
              <p>
                Company
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('superadmin/company/add') }}" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Add Company</p>
                </a>
              </li>
              <li class="nav-item">
                 <a href="{{ url('superadmin/company/list') }}" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Company List</p>
                </a>
              </li>
            
            </ul>
          </li>
		  
         <li class="nav-header">EMPLOYEES</li>
            <li class="nav-item has-treeview">
		  
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-pie-chart"></i>
              <p>
                Employee
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('superadmin/employee/add') }}" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Add Employee</p>
                </a>
              </li>
              <li class="nav-item">
                 <a href="{{ url('superadmin/employee/list') }}" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Employee List</p>
                </a>
              </li>
              
            </ul>
          </li>
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

