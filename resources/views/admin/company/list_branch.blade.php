<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
@extends('admin.admin_template')

@section('content')
  
	
		       <div class="card bg-main-content-panel">
        <div class="card-header main-content-panel-header">
          <h3>Employees</h3>

        </div>      <div class=" custom-tab"><style>.tab-padding{ padding:2%;}
		</style>
           <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab">Basic Search</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab">Advanced Search</a></li>
                
                  
                </ul>
				
				  <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                     <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->

        <!-- /.card -->

        <div class="row">
          <div class="col-md-12 ">  <div class="tab-padding">Basic Search</div></div>
		  </div>
		  </div>
		  </div>
		  
		        <div class="tab-pane" id="tab_2">
                     <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
   
        <!-- /.card -->

        <div class="row">
          <div class="col-md-12"><div class="tab-padding">Advanced Search</div></div>
		  </div>
		  </div>
		  </div>
		  
		  </div></div>
		  
		  
		  <div class="card-body main-content-panel-body">
		  
		
		
      <div class="container-fluid">  <div class="row">
          <div class="col-md-12">
              <a href="{{ route('admin.branch.add.form')}}" target="blank"> <button type="button" class="btn btn-info float-right btn-margin">+ Add</button></a>
        </div>
<style>.btn-margin{margin:0 0 10px 0;}</style>
</div>
        <div class="row">
          <div class="col-md-12">
	
		  
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Employee List</h3>    <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
			     <table class="table table-bordered">
                  <tr>
                   
                    <th>Name</th>
                    <th>Code</th>
		    <th>state</th>
                    <th></th>
                  </tr>
                  
                @forelse ($branch_list as $branch)
                  <tr>
                    <td>{{$branch->branch_name}}</td>
                    
                    <td>{{$branch->short_code}}</td>
		    <td>{{$branch->state_name}}</td>
                    <td>     
				<a href="{{ route('admin.branch.edit.form',$branch->id)}}" target="blank"><small class="badge badge-danger"><i class="fa fa-edit"></i> </small></a> &nbsp;
				<a href="#"><small class="badge badge-danger"><i class="fa fa-times"></i> </small></a></td>
                  </tr>
                @empty
                    <p>No users</p>
                  @endforelse
                </table>    
          
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                </ul>
              </div>
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
          
          <!-- /.col -->
        </div>
        <!-- /.row -->
        <!-- /.row -->
      </div><!-- /.container-fluid -->
	   <div class="">
         <button type="button" class="btn btn-info float-right"> Save</button>
        </div>
	    </div>
        <!-- /.card-body -->
        <div class="card-footer main-content-panel-footer">
        
        </div>
        <!-- /.card-footer-->
      </div>


@endsection


@section('page-js-files')

@endsection

@section('page-js-script')

@endsection
  
    