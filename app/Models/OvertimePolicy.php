<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class OvertimePolicy extends Model
{
    //
    use Notifiable;
    use SoftDeletes;
    
    protected $table = 'overtime_policy';
    
    protected $type_all = [10=>'Daily',
                             20=>'Weekly',
                             30=>'Bi Weekly',
                             40=>'Sunday',
                             50=>'Monday',
                            ];
    
    
    public function getAllTypes(){
        return $this->$type_all;
    }
}
