<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Http\Resources\Admin\EmployeeTitleResource;

use App\Models\UserTitle;

class UserTitleController extends Controller
{
    //
    
    public function __construct()
    {
            $this->middleware('auth:admin');
    }
    
    public function addForm(){
        
        $parent_user_title_data = UserTitle::where('company_id', Auth::user()->company_id)->get();
        $data = [ 'company_id'=>Auth::user()->company_id,
            'parent_user_title_data'=>$parent_user_title_data
                ];
        return view('admin.company.add_user_title',$data);
        
    }
    
    
   public function add(Request $request){
       
       
       $validator = Validator::make($request->all(), [
                        'title_name'   => 'required',
                    ]);
       
       if ($validator->fails()) {
            return redirect('admin/usertitle/add')
                        ->withErrors($validator)
                        ->withInput();
        }
       
       $user_title = new UserTitle();
       
       $user_title->company_id=$request->company_id;
       $user_title->title_name=$request->title_name;
       $user_title->parent_id=$request->parent_id;
       
        $user_title->save();
        
          return redirect('admin/usertitle/list');
    }
    
    
    
    public function userTitleList(){
        
        
        $title_list = UserTitle::where('company_id','=',Auth::user()->company_id)->get();
                
        return view('admin.company.list_user_title',['usertitle_list'=>$title_list]);
        
    }
    
    
    
    public function editForm($id){
        
        $user_title = new UserTitle();
        $title = $user_title->find($id);
        $parent_user_title_data = UserTitle::where('company_id', Auth::user()->company_id)->get();
        $data = [ 'company_id'=>Auth::user()->company_id,
            'parent_user_title_data'=>$parent_user_title_data,
            'title'=>$title,
                ];
       // return view('admin.company.edit_user_title',$data);
                  $data = view('admin.company.edit_user_title',$data)->render();
          return response()->json($data);
        
    }
    
    
   public function edit(Request $request){
       
     if($request->ajax()){  
       $validator = Validator::make($request->all(), [
                        'title_name'   => 'required',
                    ]);
       
       if ($validator->fails()) {
            return redirect('admin/usertitle/add')
                        ->withErrors($validator)
                        ->withInput();
        }
       
       $title = new UserTitle();
       $user_title = $title->find($request->id);
       
       $user_title->company_id=$request->company_id;
       $user_title->title_name=$request->title_name;
       $user_title->parent_id=$request->parent_id;
       
        $user_title->save();
        
          return redirect('admin/usertitle/list');
     }
    }
    
    
    
   public function getUserTitleForDataTable(Request $request){
       
       
       $user_title = new UserTitle();
       $query =    $user_title
                    ->where('company_id', Auth::user()->company_id)
                   // ->where('user_titles','user_titles.id' )
                    ->orderBy($request->column, $request->order);
        
        $user_title = $query->paginate($request->per_page);
        
        return EmployeeTitleResource::collection($user_title);
   }
   
   
   public function delete(Request $request,$id){
       
       $user_title = new UserTitle();
       $user_title->find($id)->delete();
        
    }
}
