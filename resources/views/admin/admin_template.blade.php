<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>EvolveHRM | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
 
   <link href="{{ asset("/plugins/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  
   <link href="{{ asset("/dist/css/adminlte.min.css") }}" rel="stylesheet" type="text/css" />
   <link href="{{ asset("/dist/css/custom.css") }}" rel="stylesheet" type="text/css" />
   <link href="{{ asset("/dist/css/theme.css") }}" rel="stylesheet" type="text/css" />
	
  <!-- iCheck -->
  <link href="{{ asset("/plugins/iCheck/flat/blue.css") }}" rel="stylesheet" type="text/css" />
 
  <!-- Morris chart -->
  <link href="{{ asset("/plugins/morris/morris.css") }}" rel="stylesheet" type="text/css" />

  <!-- jvectormap -->
  <link href="{{ asset("/plugins/jvectormap/jquery-jvectormap-1.2.2.css") }}" rel="stylesheet" type="text/css" />

  <!-- Date Picker -->
  <link href="{{ asset("/plugins/datepicker/datepicker3.css") }}" rel="stylesheet" type="text/css" />

  <!-- Daterange picker -->

  <!-- bootstrap wysihtml5 - text editor -->
  <link href="{{ asset("/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css") }}" rel="stylesheet" type="text/css" />

  
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  
  <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
 @include('admin/header')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('admin/sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
	
	 
    <section class="content">
	  @yield('content')
      </div>
    </section>
	
	  
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 @include('admin/footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ asset ("/plugins/jquery/jquery.min.js") }}"></script>


<!-- jQuery UI 1.11.4 -->

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset ("/plugins/bootstrap/js/bootstrap.bundle.min.js") }}"></script>

<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ asset ("/plugins/morris/morris.min.js") }}"></script>


<!-- Sparkline -->
<script src="{{ asset ("/plugins/sparkline/jquery.sparkline.min.js") }}"></script>


<!-- jvectormap -->
<script src="{{ asset ("/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js") }}"></script>
<script src="{{ asset ("/plugins/jvectormap/jquery-jvectormap-world-mill-en.js") }}"></script>


<!-- jQuery Knob Chart -->
<script src="{{ asset ("/plugins/knob/jquery.knob.js") }}"></script>


<!-- InputMask -->
<script src="{{ asset ("/plugins/input-mask/jquery.inputmask.js") }}"></script>
<script src="{{ asset ("/plugins/input-mask/jquery.inputmask.date.extensions.js") }}"></script>
<script src="{{ asset ("/plugins/input-mask/jquery.inputmask.extensions.js") }}"></script>

<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="{{ asset ("/plugins/daterangepicker/daterangepicker.js") }}"></script>

<!-- datepicker -->
<script src="{{ asset ("/plugins/datepicker/bootstrap-datepicker.js") }}"></script>

<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset ("/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js") }}"></script>


<!-- Slimscroll -->
<script src="{{ asset ("/plugins/slimScroll/jquery.slimscroll.min.js") }}"></script>

<!-- FastClick -->
<script src="{{ asset ("/plugins/fastclick/fastclick.js") }}"></script>


<!-- AdminLTE App -->
<script src="{{ asset ("/dist/js/adminlte.js") }}"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<!-- AdminLTE for demo purposes -->
 @yield('page-js-files')
 
 @yield('page-js-script')

</body>
</html>

