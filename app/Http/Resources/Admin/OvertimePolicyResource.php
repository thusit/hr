<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Models\OvertimePolicy;

class OvertimePolicyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $ovetime_policy_o = new OvertimePolicy();
        
        $type_array = $ovetime_policy_o->getAllTypes();
      return [
            'id'  => $this->id,
            'photo_path'  => url('storage/company/'.$this->company_name.'/'.$this->photo_path),
            'company_name'  => $this->company_name,
            'policy_name'       => $this->overtime_policy_name,
            'overtime_type'      => $type_array[$this->overtime_type],
            'active_after'    => Carbon::parse($this->active_after)->toDayDateTimeString(),
            'created_at' => Carbon::parse($this->created_at)->toDayDateTimeString(),
        ];
    }
}
