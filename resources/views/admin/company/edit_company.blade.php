<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

@extends('admin.admin_template')

@section('content')

        <div class="card-header main-content-panel-header">
          <h3>Company Information</h3>

        </div>
        <div class="card-body main-content-panel-body  custom-tab">
           
	
       
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
            
			
               
        <!-- SELECT2 EXAMPLE -->
   
        <!-- /.card -->

        <div class="row">
          <div class="col-md-6">

            <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Edit Company</h3>
				   <div class="card-tools">                
                  <button type="button" class="btn btn-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i> 
					 </button>               
                </div>
              </div>
                
         <form method="POST" action="{{ route('admin.company.edit.submit',$company->id) }}" enctype="multipart/form-data">
             @csrf <!-- {{ csrf_field() }} -->
             
             <input type="hidden" id="id" name="id" value="{{$company->id}}" />
              <div class="card-body">
			  <div class="form-group">
                    <label for="">Product Edition</label>
                       <select class="form-control  form-control-sm ">
                      <option>Standard</option>
   
                     
                    </select>
                 </div>
<div class="form-group">
                   	  
				  <label for="">Company</label>
                     <input id="company_name" type="text" class="form-control{{ $errors->has('company_name') ? ' is-invalid' : ' form-control-sm' }}" name="company_name" value="{{ $company->company_name }}" >
                  </div>
				  <div class="form-group">
                   	  
				  <label for="">Short Name</label>
                   <input id="short_name" type="text" class="form-control{{ $errors->has('short_name') ? ' is-invalid' : ' form-control-sm' }}" name="short_name" value="{{  $company->short_name }} " >
                 
                  </div>
			  <div class="form-group">
                    <label for="">Industry</label>
                    <select class="form-control  form-control-sm " name="industry" id="industry">
                        @if(count($industry_data) > 0)
                            @foreach($industry_data as $key=>$value)
                            <option value="{{$key}}"  {!! ($key==$company->industry ? "selected=\"selected\"" : "") !!} >{{$value}}</option>
                            @endForeach
                        @else
                             No Record Found
                        @endif
                     
                    </select>
                 </div>
				
   
				  <div class="form-group">
                    <label for="e">Business/Employer Identification Number</label>
                     <input id="business_identification_number" type="text" class="form-control{{ $errors->has('business_identification_number') ? ' is-invalid' : ' form-control-sm' }}" name="business_identification_number" value="{{  $company->business_identification_number }}" >
                 
               </div>
				  
				  	  <div class="form-group">
                    <label for="e">Address Line 1</label>
               <input id="address_1" type="text" class="form-control{{ $errors->has('address_1') ? ' is-invalid' : ' form-control-sm' }}" name="address_1" value="{{  $company->address_1 }}" >
                  </div>
			   	  <div class="form-group">
                    <label for="e">Address Line 2</label>
                    <input id="address_2" type="text" class="form-control{{ $errors->has('address_2') ? ' is-invalid' : ' form-control-sm' }}" name="address_2" value="{{ $company->address_2 }}" >
                  
               </div>
			   	  <div class="form-group">
                    <label for="e">City</label>
                      <input id="address_city" type="text" class="form-control{{ $errors->has('address_city') ? ' is-invalid' : ' form-control-sm' }}" name="address_city" value="{{ $company->address_city }}" >
                  
               </div>
			   		  <div class="form-group">
                    <label for="">Country</label>
                     <select class="form-control{{ $errors->has('country_id') ? ' is-invalid' : ' form-control-sm' }}" name="country_id" id="country_id" placeholder="Country">
                       @if($countries->count() > 0)
                            @foreach($countries as $country)
                            <option value="{{$country->id}}"  {!! ($country->id==$company->country_id ? "selected=\"selected\"" : "") !!} >{{$country->name}}</option>
                            @endForeach
                        @else
                             No Record Found
                        @endif
                     
                    </select>
                 </div>
				 		  <div class="form-group">
                    <label for="">Province/State</label>
                   <select class="form-control  form-control-sm" name="address_state" id="address_state">
                      <option>Western</option>
                     <option>North</option>
                     <option>South</option>
                   
                    </select>
                 </div>
				  <div class="form-group">
                    <label for="e">Zip Code</label>
                    <input id="address_postal" type="text" class="form-control{{ $errors->has('address_postal') ? ' is-invalid' : ' form-control-sm' }}" name="address_postal" value="{{ $company->address_postal }}" >
                  
               </div>
			   	  <div class="form-group">
                    <label for="e">Phone</label>
                     <input id="work_phone" type="text" class="form-control{{ $errors->has('work_phone') ? ' is-invalid' : ' form-control-sm' }}" name="work_phone" value="{{ $company->work_phone }}" >
                  
               </div>
			   	  <div class="form-group">
                    <label for="e">Fax</label>
                    <input id="fax" type="text" class="form-control{{ $errors->has('fax') ? ' is-invalid' : ' form-control-sm' }}" name="fax" value="{{ $company->fax }}" >
               
               </div>
			   	  <div class="form-group">
                    <label for="e">EPF Reg Number</label>
                      <input id="epf_number" type="text" class="form-control{{ $errors->has('epf_number') ? ' is-invalid' : ' form-control-sm' }}" name="epf_number" value="{{ $company->epf_number }}" >
               
             
               </div>
			   
			

              </div>
                    
                  
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- /.card -->

          </div>
          <!-- /.col (left) -->
          <div class="col-md-6">
		  
		  
		     <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Company Contacts</h3>
				   <div class="card-tools">                
                  <button type="button" class="btn btn-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i> 
					 </button>               
                </div>
              </div>
              <div class="card-body">
    
 
 
			    		  <div class="form-group">
                    <label for="">Administrative Contact</label>
                    <select class="form-control  form-control-sm " id="admin_contact" name="admin_contact">
                         @if($user_data->count() > 0)
                            @foreach($user_data as $user)
                            <option value="{{$user->id}}"  {!! ($user->id==$company->admin_contact ? "selected=\"selected\"" : "") !!} >{{$user->first_name.' '.$user->last_name}}</option>
                            @endForeach
                        @else
                             No Record Found
                        @endif
                     
                    </select>
                 </div>
				  		  <div class="form-group">
                    <label for="">Billing Contact</label>
                    <select class="form-control  form-control-sm " id="billing_contact" name="billing_contact">
                       @if($user_data->count() > 0)
                            @foreach($user_data as $user)
                            <option value="{{$user->id}}"  {!! ($user->id==$company->billing_contact ? "selected=\"selected\"" : "") !!} >{{$user->first_name.' '.$user->last_name}}</option>
                            @endForeach
                        @else
                             No Record Found
                        @endif
                     
                    </select>
                 </div>
				  		  <div class="form-group">
                    <label for="">Primary Support Contact</label>
                    <select class="form-control  form-control-sm " id="support_contact" name="support_contact">
                      @if($user_data->count() > 0)
                            @foreach($user_data as $user)
                            <option value="{{$user->id}}"  {!! ($user->id==$company->support_contact ? "selected=\"selected\"" : "") !!} >{{$user->first_name.' '.$user->last_name}}</option>
                            @endForeach
                        @else
                             No Record Found
                        @endif
                     
                    </select>
                 </div>

    
            </div></div> 
              
              
             <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Direct Diposit(EFT)</h3>
				   <div class="card-tools">                
                  <button type="button" class="btn btn-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i> 
					 </button>               
                </div>
              </div>
              <div class="card-body">
    <div class="form-group">
      <label for="validationCustom01">Originator ID/ Immediate Origin</label>
      <input id="originator_id" type="text" class="form-control{{ $errors->has('originator_id') ? ' is-invalid' : ' form-control-sm' }}" name="originator_id" value="{{ $company->originator_id }}" >
 </div>
 
 <div class="form-group">
      <label for="validationCustom02">Data Center/Immediate Destination</label>
     <input id="data_center_id" type="text" class="form-control{{ $errors->has('data_center_id') ? ' is-invalid' : ' form-control-sm' }}" name="data_center_id" value="{{ $company->data_center_id }}" >
 </div>

      
  
          
               
			  
			  
              <!-- /.card-body -->
            </div></div>
			
			  <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Company Settings</h3>
				   <div class="card-tools">                
                  <button type="button" class="btn btn-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i> 
					 </button>               
                </div>
              </div>
              <div class="card-body">
  

      
  
           <div class="form-group">
    <label >Logo</label>
	<div class="">
                     <div id="EmPic"><img src="{{url('storage/company/'.$company->company_name. '/'.$company->logo_path)}}" /><br><br>
</div>

					 <a href="#">Delete</a><br>
<br>


                    </div>
     <input type="file" class="form-control-file" id="company_photo" name="company_photo">
  </div>     
                <div class="form-group" style="display:">
                    <label>Second Surname</label>

                    
                    <!-- /.input group -->
                  </div>
                 <div class="form-group">
			 <div class="form-check">
                                  <input id="enable_second_surname" type="hidden" class="form-control{{ $errors->has('enable_second_surname') ? ' is-invalid' : ' form-control-sm' }}" name="enable_second_surname" value="0" >

                             <input id="enable_second_surname" type="checkbox" class="form-control{{ $errors->has('enable_second_surname') ? ' is-invalid' : ' form-control-sm' }}" name="enable_second_surname" value="1" >
                        
                    <label class="form-check-label" for="enable_second_surname">Enable Second Surname</label>
                  </div> 
				  </div>

			  
              <!-- /.card-body -->
            </div></div>
			
			  <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">LDAP Authentication</h3>
				   <div class="card-tools">                
                  <button type="button" class="btn btn-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i> 
					 </button>               
                </div>
              </div>
              <div class="card-body">
   
                
                 
				 <div class="form-group"> <label>LDAP Authentication</label>
                                     <select class="form-control  form-control-sm" name="ldpa_authentication_type" id="ldpa_authentication_type">
                                         <option value="0" {!! ($company->ldpa_authentication_type ==0 ? "selected=\"selected\"" : "") !!}>Disabled</option>
                                         <option value="1" {!! ($company->ldpa_authentication_type ==1 ? "selected=\"selected\"" : "") !!}>Enabled</option>


                                      </select>
                                 </div>
               
			  
			  
              <!-- /.card-body -->
            </div></div>
            <!-- /.card -->

      
          </div>
          <!-- /.col (right) -->
        </div>
        <!-- /.row -->
     <button type="submit" class="btn btn-info float-right"> Save</button>
                  
        
		         
		 
	  </form>	 
		 
		 
        </div>


@endsection


@section('page-js-files')

@endsection

@section('page-js-script')

@endsection

