<?php

namespace App\Http\Controllers\admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use DateTime;
use Carbon\Carbon;
use Auth;

use App\Http\Resources\Admin\EmployeeResource;

use App\Models\User;
use App\Models\Role;
use App\models\Country;
use App\Models\Branch;
use App\Models\Department;

class EmployeeController extends Controller
{
    //
    
    
    public function __construct()
    {
            $this->middleware('auth:admin');
    }
    
    
    
    public function addForm()
    {
        
        $role = new Role();
        $role_data = $role->where('name', '!=' , 'Super Administrator')->get();
        
        $country = new Country();
        $contry_data= $country->all();
        
        $branch_list = Branch::where('company_id','=',Auth::user()->company_id)->get();
        
        $deparment_o = new Department();
       
        $filename= 'avatar.png';
        $filepath = $filename;
        
        $data = [   'roles' =>$role_data,
                    'profile_pic'=>$filepath,
                    'countries'=>$contry_data,
                    'branchers'=>$branch_list,
                ];
        
       return view('admin.employee.add_employee',$data);
    }
    
    
    public function add(Request $request)
    {
       // print_r($request->input());
       
         $validator = Validator::make($request->all(), [
            'empno'   => 'required|min:6',
            'title'=>'required',
            'first_name'   => 'required',
            'last_name'   => 'required',
            'name_with_initials'   => 'required',
            'profile_photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'office_email' =>'required|email',
            'user_name'   => 'required',
            'password'   => 'required|min:6|confirmed',
            'role_id'   => 'required',
            'personal_email'=>'sometimes|nullable|email',
            'appointment_date'=>'required',
        ]);
        
      
        
         if ($validator->fails()) {
            return redirect('admin/employee/add')
                        ->withErrors($validator)
                        ->withInput();
        }
        
       $user = new User();
        
       if(isset($request->profile_photo) && !empty($request->profile_photo) && $request->file('profile_photo')->isValid()){
            $image_name = time();
            $image_name =$image_name.'.png';
            $imagePath = $request->profile_photo->storeAs('public', $image_name);
            
            $image = Image::make(Storage::get($imagePath))->resize(200,160)->encode();
            Storage::put($imagePath,$image);
    
            
            $user->photo_path = $image_name;
       }
       $date_birth = new DateTime($request->dob);
       
        $c_date = new Carbon();
       
       
      
       $user->company_id = Auth::user()->company_id;
       $user->empno = $request->empno;
       $user->title = $request->title;
       $user->first_name = $request->first_name;
       $user->last_name = $request->last_name;
       $user->name_with_initials = $request->name_with_initials;
       $user->dob = $date_birth->getTimestamp();
       $user->dob_s = $request->dob;
       $user->nic = $request->nic;
       $user->email = $request->office_email;
       $user->office_email = $request->office_email;
       $user->user_name = $request->user_name;
       $user->password = bcrypt($request->password);
       $user->gender = $request->gender;
       $user->religion = $request->religion;
       $user->marital_status = $request->marital_status;
       $user->address_1 = $request->address_1;
       $user->address_2 = $request->address_2;
       $user->address_city = $request->address_city;
       $user->address_state = $request->address_state;
       $user->address_postal = $request->address_postal;
       $user->home_phone = $request->home_phone;
       $user->mobile_phone = $request->mobile_phone;
       $user->personal_email = $request->personal_email;
       $user->office_phone = $request->office_phone;
       $user->office_phone_extention = $request->office_phone_extention;
       $user->office_mobile = $request->office_mobile;
       $user->fax = $request->fax;
       $user->city = $request->city;
       $user->country = $request->country;
       $user->state_id = $request->state_id;
       $user->department_id = $request->department_id;
       $user->branch_id = $request->branch_id;
       $user->epf_registration_no = $request->epf_registration_no;
       $user->epf_membeship_no = $request->epf_membeship_no;
       $user->emergency_contact_person = $request->emergency_contact_person;
       $user->emergency_contact_number = $request->emergency_contact_number;
       $user->appointment_date = $request->appointment_date;
       $user->appointment_note = $request->appointment_note;
       $user->terminate_date = $request->terminate_date;
       $user->terminate_note = $request->terminate_note;
       $user->confirmed_date = $request->confirmed_date;
       $user->resign_date = $request->resign_date;
       $user->role_id = $request->role_id;
       
       $user->save();
       
       
       $role_user = new RoleUser();
       
       $role_user->user_id = $user->id;
       $role_user->role_id = $request->role_id; 
       
       $role_user->save();
       
        return redirect('admin/employee/list');
    }
    
    
    
    
    
    public function employeeList(){
        
        $user = new User();
        $user_data = $user
                
                ->join('roles', 'users.role_id', '=', 'roles.id')
                ->join('companies', 'users.company_id', '=', 'companies.id')
                ->where('companies.id', Auth::user()->company_id)
                ->where('roles.name', '!=' , 'Super Administrator')->get();
        
       return view('admin.employee.list_employee',['userlist' =>$user_data]);
;
    }
    
    
    public function getEmployeeForDataTables(Request $request){
        
             $user = new User();
             $query = $user
                
                ->join('roles', 'users.role_id', '=', 'roles.id')
                ->join('companies', 'users.company_id', '=', 'companies.id')
                ->where('companies.id', Auth::user()->company_id)
                ->where('roles.name', '!=' , 'Super Administrator')
                ->orderBy($request->column, $request->order);
        
        $user_data = $query->paginate($request->per_page);
       
        return EmployeeResource::collection($user_data);
    }
    
}
