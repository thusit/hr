<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SuperAdminController extends Controller
{
    //
    public function __construct()
    {
            $this->middleware('auth:superadmin');
            $this->middleware('role:Super Administrator');
    }
    
    
    public function index()
    {
       return view('superadmin.superadmin_template');
    }
}
