<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id');
            $table->string('title');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('name_with_initials');
            $table->string('email', 128)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('user_name')->unique()->before('password');
            $table->string('password');
            $table->unsignedBigInteger('role_id');
            $table->string('empno',25)->nullable();
            $table->string('punch_machine_user_id',50)->nullable();
            $table->string('nic',25)->nullable();
            $table->integer('dob')->nullable();
            $table->string('dob_s',30)->nullable();
            $table->string('gender')->nullable();
            $table->string('religion')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('address_city')->nullable();
            $table->string('address_state')->nullable();
            $table->string('address_postal')->nullable();
            $table->string('photo_path')->nullable();
            $table->string('home_phone')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->string('personal_email')->nullable();
            $table->string('office_phone')->nullable();
            $table->string('office_phone_extention')->nullable();
            $table->string('office_mobile')->nullable();
            $table->string('office_email')->nullable();
            $table->string('fax')->nullable();
            $table->string('city')->nullable();
            $table->unsignedBigInteger('country_id');
            $table->unsignedBigInteger('state_id');
            $table->unsignedBigInteger('branch_id');
            $table->unsignedBigInteger('department_id');
            $table->string('epf_registration_no')->nullable();
            $table->string('epf_membeship_no')->nullable();
            $table->string('emergency_contact_person')->nullable();
            $table->string('emergency_contact_number')->nullable();
            $table->decimal('longitude')->nullable();
            $table->decimal('latitude')->nullable();
            $table->date('appointment_date');
            $table->string('appointment_note')->nullable();
            $table->date('terminate_date')->nullable();
            $table->string('terminate_note')->nullable();
            $table->date('confirmed_date')->nullable();
            $table->date('resign_date')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('branch_id')->references('id')->on('branch');
            $table->foreign('department_id')->references('id')->on('department');
            
        });
        
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
